package de.sybit.facades.impl;

import com.google.common.io.BaseEncoding;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.sybit.converters.AuthenticatorResponseConverter;
import de.sybit.converters.GetAssertionResponseConverter;
import de.sybit.daos.WebAuthnCredentialDao;
import de.sybit.hybrisgen.webauthn.data.AttestationData;
import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.AttestationStatement;
import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorData;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorResponseData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.FidoU2fAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.GetAssertionResponse;
import de.sybit.hybrisgen.webauthn.data.MakeCredentialsResponse;
import de.sybit.hybrisgen.webauthn.data.PackedAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialCreateOptionsData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialRequestOptions;
import de.sybit.model.WebAuthnCredentialModel;
import de.sybit.populators.AllowedCredentialPopulator;
import de.sybit.services.AttestationStatementVerificationService;
import de.sybit.services.AuthenticatorVerificationService;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static de.hybris.platform.testframework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
public class DefaultWebAuthnCredentialsFacadeTest {

    private static final String RP_NAME = "RP_NAME";
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();
    private static final int CHALLENGE_LENGTH = 32;
    private static final String RP_NAME_KEY = "webauthn.rp.name";
    private static final String RP_ID_KEY = "webauthn.rp.id";
    private static final String WEBAUTHN_TIMEOUT_CODE = "webauthn.timeout";
    private static final String WEBAUTHN_CREATE_TYPE = "webauthn.create";
    private static final String WEBAUTHN_GET_TYPE = "webauthn.get";
    private static final String WEBAUTHN_ATTESTATION_TYPE_CODE = "webauthn.attestation.type";
    private static final String RP_ID = "RP_ID";
    private static final String USER_HEX = "USER_HEX";
    private static final String USER_NAME = "USER_NAME";
    private static final String USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
    private static final String PUBLIC_KEY = "public-key";
    private static final int ALG = -7;
    private static final String CROSS_PLATFORM = "cross-platform";
    private static final long WEBAUTHN_TIMEOUT = 10000L;
    private static final String WEBAUTHN_ATTESTATION_TYPE = "direct";
    private static final byte[] publicKeyBytes = "V0VCQVVUSE5GQUNBREVURVNU".getBytes();
    private static final String CREDENTIAL_ID = "CREDENTIAL_ID";

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private UserService userService;

    @Mock
    private SessionService sessionService;

    @Mock
    private AuthenticatorResponseConverter authenticatorResponseConverter;

    @Mock
    private AuthenticatorVerificationService verificationService;

    @Mock
    private ModelService modelService;

    @Mock
    private AllowedCredentialPopulator allowedCredentialPopulator;

    @Mock
    private GetAssertionResponseConverter getAssertionResponseConverter;

    @Mock
    private WebAuthnCredentialDao webAuthnCredentialDao;

    @Mock
    private AttestationStatementVerificationService attestationStatementVerificationService;

    @InjectMocks
    private DefaultWebAuthnCredentialsFacade webAuthnCredentialsFacade;

    @Mock
    private UserModel userModel;

    @Mock
    private List<WebAuthnCredentialModel> credentials;

    @Mock
    private Configuration configuration;

    @Mock
    private Errors errorObj;

    @Mock
    private PublicKeyCredential publicKeyCredential;

    @Mock
    private MakeCredentialsResponse makeCredentialsResponse;

    @Mock
    private GetAssertionResponse getAssertionResponse;

    @Mock
    private AttestationObject attestationObject;

    @Mock
    private FieldError fieldError;

    @Mock
    private AttestationData attestationData;

    @Mock
    private AuthenticatorData authenticatorData;

    @Mock
    private AuthenticationResponseData authenticationResponseData;

    @Mock
    private CollectedClientData collectedClientData;

    @Mock
    private WebAuthnCredentialModel webAuthnCredentialModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(userService.getCurrentUser()).thenReturn(userModel);
        when(userModel.getPk()).thenReturn(PK.BIG_PK);
        when(userModel.getName()).thenReturn(USER_NAME);
        when(userModel.getDisplayName()).thenReturn(USER_DISPLAY_NAME);
        when(publicKeyCredential.getResponse()).thenReturn(makeCredentialsResponse);
        when(makeCredentialsResponse.getAttestationObject()).thenReturn(attestationObject);
        when(makeCredentialsResponse.getClientData()).thenReturn(collectedClientData);
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(RP_ID_KEY)).thenReturn(RP_ID);
        when(configuration.getString(RP_NAME_KEY)).thenReturn(RP_NAME);
        when(configuration.getLong(WEBAUTHN_TIMEOUT_CODE, 10000L)).thenReturn(WEBAUTHN_TIMEOUT);
        when(configuration.getString(WEBAUTHN_ATTESTATION_TYPE_CODE, "direct")).thenReturn(WEBAUTHN_ATTESTATION_TYPE);
    }

    @Test
    public void getWebAuthnCredentialsFromCurrentUser_hasWebAuthnCredentials_returns() {
        when(userModel.getWebAuthnCredentials()).thenReturn(credentials);

        assertEquals(credentials, webAuthnCredentialsFacade.getWebAuthnCredentialsFromCurrentUser());
    }

    @Test
    public void getWebAuthnCredentialsFromCurrentUser_hasNoWebAuthnCredentials_returnsNull() {
        when(userModel.getWebAuthnCredentials()).thenReturn(null);

        assertEquals(null, webAuthnCredentialsFacade.getWebAuthnCredentialsFromCurrentUser());
    }

    @Test
    public void makeCredentialCreateOptions() {
        final PublicKeyCredentialCreateOptionsData createOptionsData = webAuthnCredentialsFacade.makeCredentialCreateOptions();

        assertEquals(CHALLENGE_LENGTH, createOptionsData.getChallenge().length);
        assertEquals(RP_ID, createOptionsData.getRp().getId());
        assertEquals(RP_NAME, createOptionsData.getRp().getName());
        assertEquals(PK.BIG_PK.getHex(), createOptionsData.getUser().getId());
        assertEquals(USER_NAME, createOptionsData.getUser().getName());
        assertEquals(USER_DISPLAY_NAME, createOptionsData.getUser().getDisplayName());
        assertEquals(PUBLIC_KEY, createOptionsData.getPubKeyCredParams().get(0).getType());
        assertEquals(ALG, createOptionsData.getPubKeyCredParams().get(0).getAlg());
        assertEquals(CROSS_PLATFORM, createOptionsData.getAuthenticatorSelection().getAuthenticatorAttachment());
        assertEquals(WEBAUTHN_TIMEOUT, createOptionsData.getTimeout());
        assertEquals(WEBAUTHN_ATTESTATION_TYPE, createOptionsData.getAttestation());
        verify(sessionService).setAttribute("challenge", createOptionsData.getChallenge());
        verify(sessionService).setAttribute("pubKeyCredParams", createOptionsData.getPubKeyCredParams());
    }

    @Test
    public void finishCredentialRegistration_fidoU2fAttestationStatement_return_true() {
        final AuthenticatorResponseData authenticatorResponseData = new AuthenticatorResponseData();
        final FidoU2fAttestationStatement attestationStatement = new FidoU2fAttestationStatement();
        when(authenticatorResponseConverter.convert(authenticatorResponseData)).thenReturn(publicKeyCredential);
        when(attestationObject.getAttStmt()).thenReturn(attestationStatement);

        assertTrue(webAuthnCredentialsFacade.finishCredentialRegistration(authenticatorResponseData, errorObj));

        verify(verificationService).verifyClientData(publicKeyCredential.getResponse().getClientData(), WEBAUTHN_CREATE_TYPE, errorObj);
        verify(verificationService).verifyAttestationObject(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject(), errorObj);
        verify(attestationStatementVerificationService).verifyFidoU2fAttestationStatement(attestationStatement, publicKeyCredential, errorObj);
        verify(sessionService).setAttribute("publicKeyCredential", publicKeyCredential);
    }

    @Test
    public void finishCredentialRegistration_packedAttestationStatement_return_true() {
        final AuthenticatorResponseData authenticatorResponseData = new AuthenticatorResponseData();
        final PackedAttestationStatement attestationStatement = new PackedAttestationStatement();
        when(authenticatorResponseConverter.convert(authenticatorResponseData)).thenReturn(publicKeyCredential);
        when(attestationObject.getAttStmt()).thenReturn(attestationStatement);

        assertTrue(webAuthnCredentialsFacade.finishCredentialRegistration(authenticatorResponseData, errorObj));

        verify(verificationService).verifyClientData(publicKeyCredential.getResponse().getClientData(), WEBAUTHN_CREATE_TYPE, errorObj);
        verify(verificationService).verifyAttestationObject(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject(), errorObj);
        verify(attestationStatementVerificationService).verifyPackedAttestationStatement(attestationStatement, publicKeyCredential, errorObj);
        verify(sessionService).setAttribute("publicKeyCredential", publicKeyCredential);
    }

    @Test
    public void finishCredentialRegistration_noneAttestationStatement_return_true() {
        final AuthenticatorResponseData authenticatorResponseData = new AuthenticatorResponseData();
        final AttestationStatement attestationStatement = new AttestationStatement();
        when(authenticatorResponseConverter.convert(authenticatorResponseData)).thenReturn(publicKeyCredential);
        when(attestationObject.getAttStmt()).thenReturn(attestationStatement);

        assertTrue(webAuthnCredentialsFacade.finishCredentialRegistration(authenticatorResponseData, errorObj));

        verify(verificationService).verifyClientData(publicKeyCredential.getResponse().getClientData(), WEBAUTHN_CREATE_TYPE, errorObj);
        verify(verificationService).verifyAttestationObject(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject(), errorObj);
        verify(attestationStatementVerificationService).verifyNoneAttestationStatement(attestationStatement, publicKeyCredential, errorObj);
        verify(sessionService).setAttribute("publicKeyCredential", publicKeyCredential);
    }

    @Test
    public void finishCredentialRegistration_hasErrors_return_false() {
        final AuthenticatorResponseData authenticatorResponseData = new AuthenticatorResponseData();
        final AttestationStatement attestationStatement = new AttestationStatement();
        when(authenticatorResponseConverter.convert(authenticatorResponseData)).thenReturn(publicKeyCredential);
        when(attestationObject.getAttStmt()).thenReturn(attestationStatement);
        when(errorObj.hasErrors()).thenReturn(true);
        when(errorObj.getFieldError()).thenReturn(fieldError);
        when(fieldError.toString()).thenReturn("Error");

        assertFalse(webAuthnCredentialsFacade.finishCredentialRegistration(authenticatorResponseData, errorObj));

        verify(verificationService).verifyClientData(publicKeyCredential.getResponse().getClientData(), WEBAUTHN_CREATE_TYPE, errorObj);
        verify(verificationService).verifyAttestationObject(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject(), errorObj);
        verify(sessionService, never()).setAttribute("publicKeyCredential", publicKeyCredential);
    }

    @Test
    public void storeCredentialsToCurrentUser_saveToUser() {
        when(publicKeyCredential.getResponse()).thenReturn(makeCredentialsResponse);
        when(makeCredentialsResponse.getAttestationObject()).thenReturn(attestationObject);
        when(attestationObject.getAuthData()).thenReturn(authenticatorData);
        when(authenticatorData.getAttData()).thenReturn(attestationData);
        when(attestationData.getPublicKeyBytes()).thenReturn(publicKeyBytes);

        webAuthnCredentialsFacade.storeCredentialsToCurrentUser(publicKeyCredential, "title");

        verify(userModel).setWebAuthnCredentials(any());
        verify(modelService).save(userModel);
    }

    @Test
    public void deleteCredentialFromCurrentUser_saveToUser() {
        final WebAuthnCredentialModel credentialModel = new WebAuthnCredentialModel();
        credentialModel.setCredentialID(CREDENTIAL_ID);
        final List<WebAuthnCredentialModel> userCredentials = Collections.singletonList(credentialModel);

        webAuthnCredentialsFacade.deleteCredentialFromCurrentUser(CREDENTIAL_ID);

        verify(modelService).save(userModel);
    }

    @Test
    public void makeCredentialAssertionOptions_return_requestOptions() {
        final WebAuthnCredentialModel credentialModel = new WebAuthnCredentialModel();
        final List<WebAuthnCredentialModel> webAuthnCredentials = Collections.singletonList(credentialModel);
        when(userModel.getWebAuthnCredentials()).thenReturn(webAuthnCredentials);

        final PublicKeyCredentialRequestOptions credentialRequestOptions = webAuthnCredentialsFacade.makeCredentialAssertionOptions();

        assertEquals(CHALLENGE_LENGTH, credentialRequestOptions.getChallenge().length);
        assertEquals(1, credentialRequestOptions.getAllowCredentials().size());
        assertEquals(WEBAUTHN_TIMEOUT, credentialRequestOptions.getTimeout());
        verify(sessionService).setAttribute("challenge", credentialRequestOptions.getChallenge());
    }

    @Test
    public void verifyGetAssertion_noErrors_returnCredential() {
        when(getAssertionResponseConverter.convert(authenticationResponseData)).thenReturn(publicKeyCredential);
        when(errorObj.hasErrors()).thenReturn(false);

        final PublicKeyCredential credential = webAuthnCredentialsFacade.verifyGetAssertion(authenticationResponseData, errorObj);

        assertEquals(publicKeyCredential, credential);
        verify(verificationService).verifyClientData(collectedClientData, WEBAUTHN_GET_TYPE, errorObj);
        verify(verificationService).verifyGetAssertion(publicKeyCredential, errorObj);
    }

    @Test
    public void verifyGetAssertion_hasErrors_returnNull() {
        when(getAssertionResponseConverter.convert(authenticationResponseData)).thenReturn(publicKeyCredential);
        when(errorObj.hasErrors()).thenReturn(true);
        when(errorObj.getFieldError()).thenReturn(fieldError);

        final PublicKeyCredential credential = webAuthnCredentialsFacade.verifyGetAssertion(authenticationResponseData, errorObj);

        assertEquals(null, credential);
        verify(verificationService).verifyClientData(collectedClientData, WEBAUTHN_GET_TYPE, errorObj);
        verify(verificationService).verifyGetAssertion(publicKeyCredential, errorObj);
    }

    @Test
    public void finishGetAssertion_signCountGreater_returnCredentialId() {
        when(publicKeyCredential.getResponse()).thenReturn(getAssertionResponse);
        when(getAssertionResponse.getAuthenticatorData()).thenReturn(authenticatorData);
        when(authenticatorData.getSignCount()).thenReturn(2);
        when(publicKeyCredential.getId()).thenReturn(CREDENTIAL_ID);
        when(webAuthnCredentialDao.findCredentialByCredentialId(CREDENTIAL_ID)).thenReturn(Optional.of(webAuthnCredentialModel));
        when(webAuthnCredentialModel.getSignCount()).thenReturn(1);

        final String credentialId = webAuthnCredentialsFacade.finishGetAssertion(publicKeyCredential);

        assertEquals(CREDENTIAL_ID, credentialId);
        verify(webAuthnCredentialDao).findCredentialByCredentialId(CREDENTIAL_ID);
        verify(webAuthnCredentialModel).setSignCount(2);
        verify(modelService).save(webAuthnCredentialModel);
    }

    @Test
    public void finishGetAssertion_noStoredCredential_returnCredentialId() {
        when(publicKeyCredential.getResponse()).thenReturn(getAssertionResponse);
        when(getAssertionResponse.getAuthenticatorData()).thenReturn(authenticatorData);
        when(authenticatorData.getSignCount()).thenReturn(2);
        when(publicKeyCredential.getId()).thenReturn(CREDENTIAL_ID);
        when(webAuthnCredentialDao.findCredentialByCredentialId(CREDENTIAL_ID)).thenReturn(Optional.empty());

        final String credentialId = webAuthnCredentialsFacade.finishGetAssertion(publicKeyCredential);

        assertEquals(credentialId, credentialId);
        verify(webAuthnCredentialDao).findCredentialByCredentialId(CREDENTIAL_ID);
        verify(webAuthnCredentialModel, never()).setSignCount(any());
        verify(modelService, never()).save(webAuthnCredentialModel);
    }

    @Test
    public void finishGetAssertion_signCountNotGreater_returnCredentialId() {
        when(publicKeyCredential.getResponse()).thenReturn(getAssertionResponse);
        when(getAssertionResponse.getAuthenticatorData()).thenReturn(authenticatorData);
        when(authenticatorData.getSignCount()).thenReturn(0);
        when(publicKeyCredential.getId()).thenReturn(CREDENTIAL_ID);
        when(webAuthnCredentialDao.findCredentialByCredentialId(CREDENTIAL_ID)).thenReturn(Optional.of(webAuthnCredentialModel));
        when(webAuthnCredentialModel.getSignCount()).thenReturn(1);

        final String credentialId = webAuthnCredentialsFacade.finishGetAssertion(publicKeyCredential);

        assertEquals(CREDENTIAL_ID, credentialId);
        verify(webAuthnCredentialDao).findCredentialByCredentialId(CREDENTIAL_ID);
        verify(webAuthnCredentialModel, never()).setSignCount(any());
        verify(modelService, never()).save(webAuthnCredentialModel);
    }
}