package de.sybit.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.sybit.hybrisgen.webauthn.data.AllowedCredential;
import de.sybit.model.WebAuthnCredentialModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static de.hybris.platform.testframework.Assert.assertEquals;

@UnitTest
public class AllowedCredentialPopulatorTest {

    private static final String CREDENTIAL_ID = "CREDENTIAL_ID";
    private static final String TYPE = "TYPE";
    private static final List<String> TRANSPORTS = Collections.singletonList("TRANSPORT");

    @InjectMocks
    private AllowedCredentialPopulator allowedCredentialPopulator;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void populate_filledAllowedCredential() {
        final WebAuthnCredentialModel source = new WebAuthnCredentialModel();
        source.setCredentialID(CREDENTIAL_ID);
        source.setType(TYPE);
        source.setTransports(TRANSPORTS);
        final AllowedCredential target = new AllowedCredential();

        allowedCredentialPopulator.populate(source, target);

        assertEquals(CREDENTIAL_ID, target.getId());
        assertEquals(TYPE, target.getType());
        assertEquals(TRANSPORTS, target.getTransports());
    }
}