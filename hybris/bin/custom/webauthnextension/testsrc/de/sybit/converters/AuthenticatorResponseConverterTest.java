package de.sybit.converters;

import com.google.common.io.BaseEncoding;
import de.hybris.bootstrap.annotations.UnitTest;
import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorResponseData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.CredentialResponse;
import de.sybit.hybrisgen.webauthn.data.MakeCredentialsResponse;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.services.PublicKeyCredentialDecodeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.InvalidObjectException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
public class AuthenticatorResponseConverterTest {

    private static final String ID = "IDTEST";
    private static final String RAW_ID = "SURURVNU";
    private static final String TYPE = "TYPE";
    private static final String CLIENT_DATA_JSON = "Q0xJRU5UREFUQUpTT05URVNU";
    private static final String ATTESTATION_OBJECT = "QVRURVNUQVRJT05PQkpFQ1RURVNU";
    private static final List<String> TRANSPORTS = Collections.singletonList("TRANSPORT");

    private static final BaseEncoding urlEncoder = BaseEncoding.base64Url().omitPadding();
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();

    private static final byte[] RAW_ID_DECODED = urlEncoder.decode(ID);
    private static final byte[] CLIENT_DATA_JSON_DECODED = baseEncoder.decode(CLIENT_DATA_JSON);
    private static final byte[] ATTESTATION_OBJECT_DECODED = baseEncoder.decode(ATTESTATION_OBJECT);

    @Mock
    private PublicKeyCredentialDecodeService decodeService;

    @InjectMocks
    private AuthenticatorResponseConverter converter;

    @Mock
    private CollectedClientData collectedClientData;

    @Mock
    private AttestationObject attestationObject;

    @Before
    public void setUp() throws InvalidObjectException {
        MockitoAnnotations.initMocks(this);

        when(decodeService.collectClientData(CLIENT_DATA_JSON_DECODED)).thenReturn(collectedClientData);
        when(decodeService.decodeAttestationObject(ATTESTATION_OBJECT_DECODED)).thenReturn(attestationObject);
    }

    @Test
    public void convert_filledAuthenticatorResponse() throws InvalidObjectException {
        final AuthenticatorResponseData data = buildAuthenticatorResponseData();

        final PublicKeyCredential credential = converter.convert(data);

        final MakeCredentialsResponse response = (MakeCredentialsResponse) credential.getResponse();
        assertEquals(data.getId(), credential.getId());
        assertArrayEquals(RAW_ID_DECODED, credential.getRawId());
        assertEquals(data.getType(), credential.getType());
        assertEquals(data.getResponse().getTransports(), response.getTransports());
        assertEquals(collectedClientData, response.getClientData());
        assertEquals(attestationObject, response.getAttestationObject());
        verify(decodeService).collectClientData(CLIENT_DATA_JSON_DECODED);
        verify(decodeService).decodeAttestationObject(ATTESTATION_OBJECT_DECODED);
    }

    private AuthenticatorResponseData buildAuthenticatorResponseData() {
        final CredentialResponse response = new CredentialResponse();
        response.setClientDataJSON(CLIENT_DATA_JSON);
        response.setAttestationObject(ATTESTATION_OBJECT);
        response.setTransports(TRANSPORTS);

        final AuthenticatorResponseData data = new AuthenticatorResponseData();
        data.setId(ID);
        data.setRawId(RAW_ID);
        data.setType(TYPE);
        data.setResponse(response);
        return data;
    }
}
