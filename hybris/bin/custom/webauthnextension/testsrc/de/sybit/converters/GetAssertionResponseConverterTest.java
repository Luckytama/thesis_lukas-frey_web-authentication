package de.sybit.converters;

import com.google.common.io.BaseEncoding;
import de.hybris.bootstrap.annotations.UnitTest;
import de.sybit.hybrisgen.webauthn.data.AuthenticateCredentialResponse;
import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.GetAssertionResponse;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.services.PublicKeyCredentialDecodeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.InvalidObjectException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
public class GetAssertionResponseConverterTest {

    private static final String ID = "IDTEST";
    private static final String RAW_ID = "SURURVNU";
    private static final String TYPE = "TYPE";
    private static final String CLIENT_DATA_JSON = "Q0xJRU5UREFUQUpTT05URVNU";
    private static final String AUTH_DATA = "QVRURVNUQVRJT05PQkpFQ1RURVNU";
    private static final String SIGNATURE = "U0lHTkFUVVJFVEVTVA0K";
    private static final String USER_HANDLE = "VVNFUkhBTkRMRVRFU1Q=";

    private static final BaseEncoding urlEncoder = BaseEncoding.base64Url().omitPadding();
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();

    private static final byte[] RAW_ID_DECODED = urlEncoder.decode(ID);
    private static final byte[] CLIENT_DATA_JSON_DECODED = baseEncoder.decode(CLIENT_DATA_JSON);
    private static final byte[] AUTH_DATA_DECODED = baseEncoder.decode(AUTH_DATA);
    private static final byte[] SIGNATURE_DECODED = baseEncoder.decode(SIGNATURE);
    private static final byte[] USER_HANDLE_DECODED = baseEncoder.decode(USER_HANDLE);

    @Mock
    private PublicKeyCredentialDecodeService decodeService;

    @InjectMocks
    private GetAssertionResponseConverter converter;

    @Mock
    private CollectedClientData collectedClientData;

    @Mock
    private AuthenticatorData authenticatorData;

    @Before
    public void setUp() throws InvalidObjectException {
        MockitoAnnotations.initMocks(this);

        when(decodeService.collectClientData(CLIENT_DATA_JSON_DECODED)).thenReturn(collectedClientData);
        when(decodeService.decodeAuthData(AUTH_DATA_DECODED)).thenReturn(authenticatorData);
    }

    @Test
    public void convert_filledAuthenticationResponse() throws InvalidObjectException {
        final AuthenticationResponseData data = buildAuthenticationResponseData();

        final PublicKeyCredential credential = converter.convert(data);

        final GetAssertionResponse response = (GetAssertionResponse) credential.getResponse();
        assertEquals(data.getId(), credential.getId());
        assertArrayEquals(RAW_ID_DECODED, credential.getRawId());
        assertEquals(data.getType(), credential.getType());
        assertEquals(collectedClientData, response.getClientData());
        assertEquals(authenticatorData, response.getAuthenticatorData());
        assertArrayEquals(SIGNATURE_DECODED, response.getSignature());
        assertArrayEquals(USER_HANDLE_DECODED, response.getUserHandle());
        verify(decodeService).collectClientData(CLIENT_DATA_JSON_DECODED);
        verify(decodeService).decodeAuthData(AUTH_DATA_DECODED);

    }

    private AuthenticationResponseData buildAuthenticationResponseData() {
        final AuthenticateCredentialResponse response = new AuthenticateCredentialResponse();
        response.setClientDataJSON(CLIENT_DATA_JSON);
        response.setAuthenticatorData(AUTH_DATA);
        response.setSignature(SIGNATURE);
        response.setUserHandle(USER_HANDLE);

        final AuthenticationResponseData data = new AuthenticationResponseData();
        data.setId(ID);
        data.setRawId(RAW_ID);
        data.setType(TYPE);
        data.setResponse(response);
        return data;
    }
}