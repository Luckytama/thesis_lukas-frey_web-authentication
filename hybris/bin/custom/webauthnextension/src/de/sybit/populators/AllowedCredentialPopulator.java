package de.sybit.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.sybit.hybrisgen.webauthn.data.AllowedCredential;
import de.sybit.model.WebAuthnCredentialModel;

public class AllowedCredentialPopulator implements Populator<WebAuthnCredentialModel, AllowedCredential> {

    @Override
    public void populate(WebAuthnCredentialModel source, AllowedCredential target) throws ConversionException {
        target.setId(source.getCredentialID());
        target.setType(source.getType());
        target.setTransports(source.getTransports());
    }
}
