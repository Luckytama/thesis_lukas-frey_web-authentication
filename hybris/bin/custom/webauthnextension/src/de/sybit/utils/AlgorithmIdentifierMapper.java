package de.sybit.utils;

import org.jose4j.jws.BaseSignatureAlgorithm;
import org.jose4j.jws.EcdsaUsingShaAlgorithm;
import org.jose4j.jws.RsaUsingShaAlgorithm;

import java.util.HashMap;
import java.util.Map;

public class AlgorithmIdentifierMapper {

    private AlgorithmIdentifierMapper() {}

    private static final Map<Integer, BaseSignatureAlgorithm> map = new HashMap<>();

    static {
        map.put(-7, new EcdsaUsingShaAlgorithm.EcdsaP256UsingSha256()); //"ES256"
        map.put(-35, new EcdsaUsingShaAlgorithm.EcdsaP384UsingSha384()); //"ES384"
        map.put(-36, new EcdsaUsingShaAlgorithm.EcdsaP521UsingSha512()); //"ES512"
        map.put(-257, new RsaUsingShaAlgorithm.RsaSha256()); //"RS256"
        map.put(-258, new RsaUsingShaAlgorithm.RsaSha384()); //"RS384"
        map.put(-259, new RsaUsingShaAlgorithm.RsaSha512()); //"RS512"
        map.put(-37, new RsaUsingShaAlgorithm.RsaPssSha256()); //"PS256"
        map.put(-38, new RsaUsingShaAlgorithm.RsaPssSha384()); //"PS384"
        map.put(-39, new RsaUsingShaAlgorithm.RsaPssSha512()); //"PS512"
    }

    public static BaseSignatureAlgorithm get(int algorithm) {
        return map.get(algorithm);
    }
}
