package de.sybit.facades;

import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorResponseData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialCreateOptionsData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialRequestOptions;
import de.sybit.model.WebAuthnCredentialModel;
import org.springframework.validation.Errors;

import java.util.List;

public interface WebAuthnCredentialsFacade {

    List<WebAuthnCredentialModel> getWebAuthnCredentialsFromCurrentUser();

    PublicKeyCredentialCreateOptionsData makeCredentialCreateOptions();

    boolean finishCredentialRegistration(AuthenticatorResponseData credential, Errors errorObj);

    void storeCredentialsToCurrentUser(PublicKeyCredential publicKeyCredential, String title);

    void deleteCredentialFromCurrentUser(String credentialId);

    PublicKeyCredentialRequestOptions makeCredentialAssertionOptions();

    PublicKeyCredential verifyGetAssertion(AuthenticationResponseData authenticationResponseData, Errors errorObj);

    String finishGetAssertion(PublicKeyCredential publicKeyCredential);
}
