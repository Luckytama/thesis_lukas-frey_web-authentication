package de.sybit.facades.impl;

import com.google.common.io.BaseEncoding;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.sybit.converters.AuthenticatorResponseConverter;
import de.sybit.converters.GetAssertionResponseConverter;
import de.sybit.daos.WebAuthnCredentialDao;
import de.sybit.facades.WebAuthnCredentialsFacade;
import de.sybit.hybrisgen.webauthn.data.*;
import de.sybit.model.WebAuthnCredentialModel;
import de.sybit.populators.AllowedCredentialPopulator;
import de.sybit.services.AttestationStatementVerificationService;
import de.sybit.services.AuthenticatorVerificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;

import javax.annotation.Resource;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class DefaultWebAuthnCredentialsFacade implements WebAuthnCredentialsFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultWebAuthnCredentialsFacade.class);
    private static final SecureRandom random = new SecureRandom();
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();
    private static final int CHALLENGE_LENGTH = 32;
    private static final String RP_NAME_KEY = "webauthn.rp.name";
    private static final String RP_ID_KEY = "webauthn.rp.id";
    private static final String WEBAUTHN_TIMEOUT_CODE = "webauthn.timeout";
    private static final String WEBAUTHN_CREATE_TYPE = "webauthn.create";
    private static final String WEBAUTHN_GET_TYPE = "webauthn.get";
    private static final String WEBAUTHN_ATTESTATION_TYPE_CODE = "webauthn.attestation.type";
    private static final String DEFAULT_ALGORITHMS = "-7";
    private static final String WEBAUTHN_CREDENTIAL_ALGORITHM = "webauthn.credential.algorithms";
    private static final String DEFAULT_KEY_TYPE = "public-key";
    private static final String ALGORITHM_DELIMITER = ",";
    private static final String WEBAUTHN_AUTHENTICATOR_SELECTION = "webauthn.authenticator.selection";
    private static final String DEFAULT_AUTHENTICATOR_SELECTION = "cross-platform";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "authenticatorResponseConverter")
    private AuthenticatorResponseConverter authenticatorResponseConverter;

    @Resource(name = "authenticatorVerificationService")
    private AuthenticatorVerificationService verificationService;

    @Resource(name = "modelService")
    private ModelService modelService;

    @Resource(name = "allowedCredentialPopulator")
    private AllowedCredentialPopulator allowedCredentialPopulator;

    @Resource(name = "getAssertionResponseConverter")
    private GetAssertionResponseConverter getAssertionResponseConverter;

    @Resource(name = "webAuthnCredentialDao")
    private WebAuthnCredentialDao webAuthnCredentialDao;

    @Resource(name = "attestationStatementVerificationService")
    private AttestationStatementVerificationService attestationStatementVerificationService;

    @Override
    public List<WebAuthnCredentialModel> getWebAuthnCredentialsFromCurrentUser() {
        final UserModel currentUser = userService.getCurrentUser();
        return currentUser.getWebAuthnCredentials();
    }

    @Override
    public PublicKeyCredentialCreateOptionsData makeCredentialCreateOptions() {
        final PublicKeyCredentialCreateOptionsData publicKeyCredCreateOpt = new PublicKeyCredentialCreateOptionsData();
        publicKeyCredCreateOpt.setChallenge(createChallenge());
        publicKeyCredCreateOpt.setRp(createRelyingParty());
        publicKeyCredCreateOpt.setUser(createWebAuthnUser());
        publicKeyCredCreateOpt.setPubKeyCredParams(createPubKeyCredParams());
        publicKeyCredCreateOpt.setAuthenticatorSelection(createAuthenticatorSelection());
        publicKeyCredCreateOpt.setTimeout(configurationService.getConfiguration().getLong(WEBAUTHN_TIMEOUT_CODE, 10000L));
        publicKeyCredCreateOpt.setAttestation(configurationService.getConfiguration().getString(WEBAUTHN_ATTESTATION_TYPE_CODE, "direct"));

        saveCreateOptionsToSession(publicKeyCredCreateOpt);

        return publicKeyCredCreateOpt;
    }

    @Override
    public boolean finishCredentialRegistration(final AuthenticatorResponseData responseData, final Errors errorObj) {
        PublicKeyCredential publicKeyCredential = authenticatorResponseConverter.convert(responseData);
        verificationService.verifyClientData(publicKeyCredential.getResponse().getClientData(), WEBAUTHN_CREATE_TYPE, errorObj);
        verificationService.verifyAttestationObject(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject(), errorObj);

        final AttestationStatement attStmt = ((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject().getAttStmt();

        if (attStmt instanceof FidoU2fAttestationStatement) {
            attestationStatementVerificationService.verifyFidoU2fAttestationStatement((FidoU2fAttestationStatement) attStmt, publicKeyCredential, errorObj);
        } else if (attStmt instanceof PackedAttestationStatement) {
            attestationStatementVerificationService.verifyPackedAttestationStatement((PackedAttestationStatement) attStmt, publicKeyCredential, errorObj);
        } else {
            attestationStatementVerificationService.verifyNoneAttestationStatement(attStmt, publicKeyCredential, errorObj);
        }

        if (errorObj.hasErrors()) {
            LOG.debug("Could not validate the generated Credentials from the Authenticator. Has errors in: " + Objects.requireNonNull(errorObj.getFieldError()).toString());
            return false;
        } else {
            sessionService.setAttribute("publicKeyCredential", publicKeyCredential);
            return true;
        }
    }

    @Override
    public void storeCredentialsToCurrentUser(final PublicKeyCredential publicKeyCredential, final String title) {
        final AttestationData attestationData = ((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject().getAuthData().getAttData();
        final UserModel currentUser = userService.getCurrentUser();
        final WebAuthnCredentialModel credentialModel = new WebAuthnCredentialModel();
        credentialModel.setCredentialID(publicKeyCredential.getId());
        credentialModel.setPublicKey(baseEncoder.encode(attestationData.getPublicKeyBytes()));
        credentialModel.setTitle(title);
        credentialModel.setType(publicKeyCredential.getType());
        credentialModel.setTransports(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getTransports());
        credentialModel.setSignCount(((MakeCredentialsResponse) publicKeyCredential.getResponse()).getAttestationObject().getAuthData().getSignCount());
        ArrayList<WebAuthnCredentialModel> credentials = new ArrayList<>(currentUser.getWebAuthnCredentials());
        credentials.add(credentialModel);
        currentUser.setWebAuthnCredentials(credentials);
        modelService.save(currentUser);
    }

    @Override
    public void deleteCredentialFromCurrentUser(final String credentialId) {
        final UserModel currentUser = userService.getCurrentUser();
        ArrayList<WebAuthnCredentialModel> credentials = new ArrayList<>(currentUser.getWebAuthnCredentials());
        currentUser.setWebAuthnCredentials(credentials.stream().filter(credential -> !credential.getCredentialID().equals(credentialId)).collect(Collectors.toList()));
        modelService.save(currentUser);
    }

    @Override
    public PublicKeyCredentialRequestOptions makeCredentialAssertionOptions() {
        final PublicKeyCredentialRequestOptions credentialRequestOptions = new PublicKeyCredentialRequestOptions();
        credentialRequestOptions.setChallenge(createChallenge());
        credentialRequestOptions.setAllowCredentials(buildAllowedCredentials());
        credentialRequestOptions.setTimeout(configurationService.getConfiguration().getLong(WEBAUTHN_TIMEOUT_CODE, 10000L));

        sessionService.setAttribute("challenge", credentialRequestOptions.getChallenge());

        return credentialRequestOptions;
    }

    @Override
    public PublicKeyCredential verifyGetAssertion(final AuthenticationResponseData authenticationResponseData, final Errors errorObj) {
        PublicKeyCredential publicKeyCredential = getAssertionResponseConverter.convert(authenticationResponseData);
        verificationService.verifyClientData(publicKeyCredential.getResponse().getClientData(), WEBAUTHN_GET_TYPE, errorObj);
        verificationService.verifyGetAssertion(publicKeyCredential, errorObj);

        if (errorObj.hasErrors()) {
            LOG.debug("Could not validate the generated Credentials from the Authenticator. Has errors in: " + Objects.requireNonNull(errorObj.getFieldError()).toString());
            return null;
        } else {
            return publicKeyCredential;
        }
    }

    @Override
    public String finishGetAssertion(final PublicKeyCredential publicKeyCredential) {
        updateSignCount(((GetAssertionResponse) publicKeyCredential.getResponse()).getAuthenticatorData().getSignCount(), publicKeyCredential.getId());
        return publicKeyCredential.getId();
    }


    private void updateSignCount(final int signCount, final String credentialId) {
        final Optional<WebAuthnCredentialModel> maybeStoredCredential = webAuthnCredentialDao.findCredentialByCredentialId(credentialId);
        if (maybeStoredCredential.isEmpty()) {
            return;
        }

        final WebAuthnCredentialModel credentialModel = maybeStoredCredential.get();

        if (signCount > credentialModel.getSignCount()) {
            credentialModel.setSignCount(signCount);
            modelService.save(credentialModel);
        } else {
            LOG.debug("maybe an attack");
        }
    }

    private List<AllowedCredential> buildAllowedCredentials() {
        final UserModel currentUser = userService.getCurrentUser();
        final List<WebAuthnCredentialModel> webAuthnCredentials = currentUser.getWebAuthnCredentials();
        final List<AllowedCredential> allowedCredentials = new ArrayList<>();
        for (WebAuthnCredentialModel webAuthnCredentialModel : webAuthnCredentials) {
            AllowedCredential allowedCredential = new AllowedCredential();
            allowedCredentialPopulator.populate(webAuthnCredentialModel, allowedCredential);
            allowedCredentials.add(allowedCredential);
        }
        return allowedCredentials;
    }

    private void saveCreateOptionsToSession(final PublicKeyCredentialCreateOptionsData createOptions) {
        sessionService.setAttribute("challenge", createOptions.getChallenge());
        sessionService.setAttribute("pubKeyCredParams", createOptions.getPubKeyCredParams());
    }

    private RelyingParty createRelyingParty() {
        final RelyingParty rp = new RelyingParty();
        rp.setId(configurationService.getConfiguration().getString(RP_ID_KEY));
        rp.setName(configurationService.getConfiguration().getString(RP_NAME_KEY));
        return rp;
    }

    private WebAuthnUser createWebAuthnUser() {
        final WebAuthnUser webAuthnUser = new WebAuthnUser();
        final UserModel currentUser = userService.getCurrentUser();
        webAuthnUser.setId(currentUser.getPk().getHex());
        webAuthnUser.setName(currentUser.getName());
        webAuthnUser.setDisplayName(currentUser.getDisplayName());
        return webAuthnUser;
    }

    private AuthenticatorSelection createAuthenticatorSelection() {
        final AuthenticatorSelection authenticatorSelection = new AuthenticatorSelection();
        authenticatorSelection.setAuthenticatorAttachment(configurationService.getConfiguration().getString(WEBAUTHN_AUTHENTICATOR_SELECTION, DEFAULT_AUTHENTICATOR_SELECTION));
        return authenticatorSelection;
    }

    private List<PublicKeyCredentialType> createPubKeyCredParams() {
        final List<PublicKeyCredentialType> credentialTypes = new ArrayList<>();
        final String[] algorithms = configurationService.getConfiguration().getString(WEBAUTHN_CREDENTIAL_ALGORITHM, DEFAULT_ALGORITHMS).split(ALGORITHM_DELIMITER);
        for (String alg : algorithms) {
            addCredentialType(credentialTypes, DEFAULT_KEY_TYPE, Integer.parseInt(alg));
        }
        return credentialTypes;
    }

    private void addCredentialType(final List<PublicKeyCredentialType> credentialTypes, final String type, final int algorithm) {
        final PublicKeyCredentialType credentialType = new PublicKeyCredentialType();
        credentialType.setType(type);
        credentialType.setAlg(algorithm);
        credentialTypes.add(credentialType);
    }

    private byte[] createChallenge() {
        byte[] challenge = new byte[CHALLENGE_LENGTH];
        random.nextBytes(challenge);
        return challenge;
    }
}
