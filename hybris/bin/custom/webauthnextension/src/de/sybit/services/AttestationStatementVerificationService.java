package de.sybit.services;

import de.sybit.hybrisgen.webauthn.data.AttestationStatement;
import de.sybit.hybrisgen.webauthn.data.FidoU2fAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.PackedAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import org.springframework.validation.Errors;

import java.security.PublicKey;

public interface AttestationStatementVerificationService {

    void verifyFidoU2fAttestationStatement(FidoU2fAttestationStatement attestationStatement, PublicKeyCredential credential, Errors errorObj);

    void verifyPackedAttestationStatement(PackedAttestationStatement attestationStatement, PublicKeyCredential credential, Errors errorObj);

    void verifyNoneAttestationStatement(AttestationStatement attestationStatement, PublicKeyCredential credential, Errors errorObj);

    boolean verifySignature(PublicKey pubKey, byte[] signedBytes, byte[] signature, String signatureAlgorithm);
}
