package de.sybit.services.impl;

import com.google.common.io.BaseEncoding;
import com.google.common.primitives.Bytes;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.sybit.daos.WebAuthnCredentialDao;
import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.GetAssertionResponse;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialType;
import de.sybit.model.WebAuthnCredentialModel;
import de.sybit.services.AttestationStatementVerificationService;
import de.sybit.services.AuthenticatorVerificationService;
import de.sybit.services.PublicKeyCredentialDecodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Optional;

public class DefaultAuthenticatorVerificationService implements AuthenticatorVerificationService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultAuthenticatorVerificationService.class);
    private static final Encoder urlEncoder = Base64.getUrlEncoder().withoutPadding();
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();

    private static final String CLIENT_DATA_FIELD = "response.clientDataJSON";
    private static final String ATTESTATION_OBJECT_FIELD = "response.attestationObject";
    private static final String CLIENT_DATA_TYPE_ERROR_CODE = "type.error";
    private static final String CHALLENGE_MISMATCH_ERROR_CODE = "challenge.error";
    private static final String ORIGIN_MISMATCH_ERROR_CODE = "origin.error";
    private static final String ORIGIN_ID_HASH_MISMATCH_ERROR_CODE = "origin.id.hash.error";
    private static final String NOT_SUPPORTED_ALG_ERROR_CODE = "alg.not.supported.error";
    private static final String NOT_SUPPORTED_ATTSTMT_ERROR_CODE = "attestation.statement.not.supported.error";
    private static final String REGISTERED_CREDENTIAL_ID_ERROR_CODE = "registered.credential.id.error";
    private static final String USER_NOT_PRESENT_ERROR_CODE = "user.not.present.error";
    private static final String USER_NOT_VERIFIED_ERROR_CODE = "user.not.verified.error";
    private static final String WEBAUTHN_USER_VERIFICATION_REQUIRED_CODE = "webauthn.user.verification.required";
    private static final String NO_MATCHING_CREDENTIAL_ERROR_CODE = "user.credentials.not.matching";
    private static final String ORIGIN_ID_CODE = "webauthn.rp.id";
    private static final String ORIGIN_SESSION_ATTRIBUTE = "origin";
    private static final int USER_PRESENT_BIT_POSITION_FLAG = 0;
    private static final int USER_VERIFIED_BIT_POSITION_FLAG = 2;
    private static final String CHALLENGE_SESSION_ATTRIBUTE = "challenge";
    private static final String NO_VALID_SIGNATURE_ERROR_CODE = "no.valid.signature";
    private static final String RESPONSE_SIGNATURE_FIELD = "response.signature";
    private static final String SHA256_WITH_ECDSA_ALGORITHM = "SHA256withECDSA";

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "webAuthnCredentialDao")
    private WebAuthnCredentialDao webAuthnCredentialDao;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "publicKeyCredentialDecodeService")
    private PublicKeyCredentialDecodeService publicKeyCredentialDecodeService;

    @Resource(name = "attestationStatementVerificationService")
    private AttestationStatementVerificationService attestationStatementVerificationService;

    @Override
    public void verifyClientData(final CollectedClientData clientData, final String actionType, final Errors errorObj) {

        if (!clientData.getType().equals(actionType)) {
            errorObj.rejectValue(CLIENT_DATA_FIELD, CLIENT_DATA_TYPE_ERROR_CODE, "Type must be 'webauthn.create'");
        }

        final byte[] challenge = sessionService.getAttribute(CHALLENGE_SESSION_ATTRIBUTE);
        if (!clientData.getChallenge().equals(urlEncoder.encodeToString(challenge))) {
            errorObj.rejectValue(CLIENT_DATA_FIELD, CHALLENGE_MISMATCH_ERROR_CODE, "Challenge from Authenticator does not match the created challenge");
        }
        sessionService.removeAttribute(CHALLENGE_SESSION_ATTRIBUTE);

        if (!clientData.getOrigin().equals(configurationService.getConfiguration().getString(ORIGIN_ID_CODE))) {
            errorObj.rejectValue(CLIENT_DATA_FIELD, ORIGIN_MISMATCH_ERROR_CODE, "The origin of the relying party matches not the id");
        }

        //TODO: Validate Token Binding (Step 7)
    }

    @Override
    public void verifyAttestationObject(final AttestationObject attestationObject, final Errors errorObj) {

        verifyOrigin(attestationObject.getAuthData().getRpIdHash(), errorObj);

        verifyUserPresent(attestationObject.getAuthData().getFlags(), errorObj);

        verifyUserVerified(attestationObject.getAuthData().getFlags(), errorObj);

        List<PublicKeyCredentialType> pubKeyCredParams = sessionService.getAttribute("pubKeyCredParams");
        final int algCode = attestationObject.getAuthData().getAttData().getPublicKey().getAlg();
        if (pubKeyCredParams.stream().noneMatch(param -> param.getAlg() == algCode)) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NOT_SUPPORTED_ALG_ERROR_CODE, "The Algorithm which is used for securing the public key is not in the supported algorithm list: " + algCode);
        }

        if (attestationObject.getAttStmt() == null) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NOT_SUPPORTED_ATTSTMT_ERROR_CODE, "The Attestation Statement is not a known or valid type");
        }

        if (webAuthnCredentialDao.findCredentialByCredentialId(urlEncoder.encodeToString(attestationObject.getAuthData().getAttData().getCredentialId())).isPresent()) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, REGISTERED_CREDENTIAL_ID_ERROR_CODE, "The provided credentialId is already stored in the database. MUST BE UNIQUE!");
        }

        //TODO: Execute the verification procedur for the specific attestation format(Step 16)
        //TODO: Check the given extensions as soon as this feature is implemented in the credential create options

    }

    @Override
    public void verifyGetAssertion(final PublicKeyCredential publicKeyCredential, final Errors errorObj) {
        final GetAssertionResponse getAssertionResponse = (GetAssertionResponse) publicKeyCredential.getResponse();

        verifyOrigin(getAssertionResponse.getAuthenticatorData().getRpIdHash(), errorObj);

        verifyUserPresent(getAssertionResponse.getAuthenticatorData().getFlags(), errorObj);

        verifyUserVerified(getAssertionResponse.getAuthenticatorData().getFlags(), errorObj);

        verifySignature(publicKeyCredential, getAssertionResponse, errorObj);
    }

    private void verifySignature(final PublicKeyCredential publicKeyCredential, final GetAssertionResponse getAssertionResponse, final Errors errorObj) {
        final UserModel currentUser = userService.getCurrentUser();
        final Optional<WebAuthnCredentialModel> storedCredential = currentUser.getWebAuthnCredentials().stream().filter(cred -> cred.getCredentialID().equals(publicKeyCredential.getId())).findAny();

        if (storedCredential.isEmpty()) {
            errorObj.rejectValue("id", NO_MATCHING_CREDENTIAL_ERROR_CODE, "The credentials from the user doesn't match the given credentialId.");
            return;
        }

        final byte[] publicKeyBytes = baseEncoder.decode(storedCredential.get().getPublicKey());
        final byte[] signature = getAssertionResponse.getSignature();
        byte[] signedBytes;
        final PublicKey publicKey = publicKeyCredentialDecodeService.decodeStoredPublicKey(publicKeyBytes);

        try {
            MessageDigest sha256Digset = MessageDigest.getInstance("SHA256");
            signedBytes = Bytes.concat(getAssertionResponse.getAuthenticatorData().getAuthDataBytes(), sha256Digset.digest(getAssertionResponse.getClientData().getClientDataBytes()));
            if (!attestationStatementVerificationService.verifySignature(publicKey, signedBytes, signature, SHA256_WITH_ECDSA_ALGORITHM)) {
                errorObj.rejectValue(RESPONSE_SIGNATURE_FIELD, NO_VALID_SIGNATURE_ERROR_CODE, "Cannot verify signature of response");
            }
        } catch (NoSuchAlgorithmException e) {
            errorObj.rejectValue(RESPONSE_SIGNATURE_FIELD, NO_VALID_SIGNATURE_ERROR_CODE, "Cannot verify signature of response");
        }
    }

    private void verifyUserVerified(byte flags, Errors errorObj) {
        if (configurationService.getConfiguration().getBoolean(WEBAUTHN_USER_VERIFICATION_REQUIRED_CODE, false) && ((flags >> USER_VERIFIED_BIT_POSITION_FLAG) & 1) == 0) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, USER_NOT_VERIFIED_ERROR_CODE, "The flags in authentication data represents that the user is not verified.");
        }
    }

    private void verifyUserPresent(final byte flags, Errors errorObj) {
        if (((flags >> USER_PRESENT_BIT_POSITION_FLAG) & 1) == 0) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, USER_NOT_PRESENT_ERROR_CODE, "The flags in authentication data represents that the user is not present.");
        }
    }

    private void verifyOrigin(final byte[] rpIdHash, final Errors errorObj) {
        byte[] originIdHash = null;
        try {
            MessageDigest sha256Digset = MessageDigest.getInstance("SHA256");
            final String originId = configurationService.getConfiguration().getString(ORIGIN_ID_CODE);
            originIdHash = sha256Digset.digest(originId.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            LOG.debug("Could not hash the rpOriginId: " + sessionService.getAttribute(ORIGIN_SESSION_ATTRIBUTE));
        }
        if (originIdHash == null || !Arrays.equals(originIdHash, rpIdHash)) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, ORIGIN_ID_HASH_MISMATCH_ERROR_CODE);
        }
    }

}
