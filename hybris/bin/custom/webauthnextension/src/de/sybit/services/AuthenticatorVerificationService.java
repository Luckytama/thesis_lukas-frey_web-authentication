package de.sybit.services;

import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import org.springframework.validation.Errors;

public interface AuthenticatorVerificationService {

    void verifyClientData(CollectedClientData clientData, String actionType, Errors errorObj);

    void verifyAttestationObject(AttestationObject attestationObject, Errors errorObj);

    void verifyGetAssertion(PublicKeyCredential publicKeyCredential, Errors errorObj);
}
