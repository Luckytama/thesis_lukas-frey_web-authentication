package de.sybit.services.impl;

import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.model.Array;
import co.nstant.in.cbor.model.ByteString;
import co.nstant.in.cbor.model.DataItem;
import co.nstant.in.cbor.model.Map;
import co.nstant.in.cbor.model.NegativeInteger;
import co.nstant.in.cbor.model.UnicodeString;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.sybit.hybrisgen.webauthn.data.AttestationStatement;
import de.sybit.hybrisgen.webauthn.data.FidoU2fAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.PackedAttestationStatement;
import de.sybit.services.AttestationStatementDecodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.io.InvalidObjectException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DefaultAttestationStatementDecodeService implements AttestationStatementDecodeService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultAttestationStatementDecodeService.class);
    private static final String X_5_C_LABEL = "x5c";
    private static final String SIG_LABEL = "sig";
    private static final String ALGORITHM_LABEL = "alg";
    private static final String ECDAA_KEY_ID_LABEL = "ecdaaKeyId";
    private static final String FIDOU2F_ATTESTATION_TYPE_CODE = "attestation.type.fidou2f";
    private static final String PACKED_ATTESTATION_TYPE_CODE = "attestation.type.packed";
    private static final String NONE_ATTESTATION_TYPE_CODE = "attestation.type.none";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Override
    public FidoU2fAttestationStatement encodeFidoU2fAttestationStatement(DataItem attStmt) throws InvalidObjectException {
        FidoU2fAttestationStatement fidoStatement = new FidoU2fAttestationStatement();
        fidoStatement.setType(configurationService.getConfiguration().getString(FIDOU2F_ATTESTATION_TYPE_CODE, "FIDOU2F ATTESTATION"));

        Map item = null;
        if (attStmt instanceof ByteString) {
            byte[] temp = ((ByteString) attStmt).getBytes();
            List<DataItem> dataItems = null;
            try {
                dataItems = CborDecoder.decode(temp);
            } catch (CborException e) {
                throw new InvalidObjectException("Failed to encode FidoU2fAttestationStatement");
            }
            item = (Map) dataItems.get(0);
        } else {
            item = (Map) attStmt;
        }

        for (DataItem data : item.getKeys()) {
            if (data instanceof UnicodeString) {
                switch (((UnicodeString) data).getString()) {
                    case X_5_C_LABEL:
                        Array array = (Array) item.get(data);
                        List<DataItem> list = array.getDataItems();
                        if (!list.isEmpty()) {
                            fidoStatement.setAttestnCert(((ByteString) list.get(0)).getBytes());
                        }
                        fidoStatement.setCaCert(encodeCaCert(list));
                        break;
                    case SIG_LABEL:
                        fidoStatement.setSig(((ByteString) item.get(data)).getBytes());
                        break;
                    default:
                        break;
                }
            }
        }
        return fidoStatement;
    }

    private List<byte[]> encodeCaCert(List<DataItem> list) {
        List<byte[]> caCert = new ArrayList<>();
        for (int i = 1; i < list.size(); i++) {
            caCert.add(((ByteString) list.get(i)).getBytes());
        }
        return caCert;
    }

    @Override
    public PackedAttestationStatement encodePackedAttestationStatement(DataItem attStmt) throws InvalidObjectException {
        PackedAttestationStatement packedStatement = new PackedAttestationStatement();
        packedStatement.setType(configurationService.getConfiguration().getString(PACKED_ATTESTATION_TYPE_CODE, "PACKED ATTESTATION"));

        Map item = null;
        if (attStmt instanceof ByteString) {
            byte[] temp = ((ByteString) attStmt).getBytes();
            List<DataItem> dataItems = null;
            try {
                dataItems = CborDecoder.decode(temp);
            } catch (CborException e) {
                LOG.debug("Could not decode packed attestation statement");
                throw new InvalidObjectException("Packed Attestation Statement is not correct encoded.");
            }
            item = (Map) dataItems.get(0);
        } else {
            item = (Map) attStmt;
        }

        for (DataItem data : item.getKeys()) {
            if (data instanceof UnicodeString) {
                switch (((UnicodeString) data).getString()) {
                    case X_5_C_LABEL:
                        Array array = (Array) item.get(data);
                        List<DataItem> list = array.getDataItems();
                        if (!list.isEmpty()) {
                            packedStatement.setAttestnCert(((ByteString) list.get(0)).getBytes());
                        }
                        packedStatement.setCaCert(encodeCaCert(list));
                        break;
                    case SIG_LABEL:
                        packedStatement.setSig(((ByteString) (item.get(data))).getBytes());
                        break;
                    case ALGORITHM_LABEL:
                        packedStatement.setAlg(new BigDecimal(((NegativeInteger) (item.get(data))).getValue()).intValueExact());
                        break;
                    case ECDAA_KEY_ID_LABEL:
                        packedStatement.setEcdaaKeyId(((ByteString) (item.get(data))).getBytes());
                        break;
                    default:
                        LOG.debug("Could not encode packed attestation type. Invalid key: " + ((UnicodeString) data).getString());
                        throw new InvalidObjectException("Faild to encode packed attestation statement");
                }
            }
        }
        return packedStatement;
    }

    @Override
    public AttestationStatement encodeNoneAttestationStatement() {
        AttestationStatement noneStatement = new AttestationStatement();
        noneStatement.setType(configurationService.getConfiguration().getString(NONE_ATTESTATION_TYPE_CODE, "NONE ATTESTATION"));
        return noneStatement;
    }
}
