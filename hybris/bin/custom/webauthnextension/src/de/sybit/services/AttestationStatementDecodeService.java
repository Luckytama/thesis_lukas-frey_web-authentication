package de.sybit.services;

import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.model.DataItem;
import de.sybit.hybrisgen.webauthn.data.AttestationStatement;
import de.sybit.hybrisgen.webauthn.data.FidoU2fAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.PackedAttestationStatement;

import java.io.InvalidObjectException;

public interface AttestationStatementDecodeService {

    FidoU2fAttestationStatement encodeFidoU2fAttestationStatement(DataItem attStmt) throws CborException, InvalidObjectException;

    PackedAttestationStatement encodePackedAttestationStatement(DataItem attStmt) throws InvalidObjectException;

    AttestationStatement encodeNoneAttestationStatement();
}
