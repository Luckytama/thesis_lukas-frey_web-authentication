package de.sybit.services.impl;

import com.google.common.primitives.Bytes;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.sybit.hybrisgen.webauthn.data.AttestationData;
import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.AttestationStatement;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.CredentialPublicKey;
import de.sybit.hybrisgen.webauthn.data.ECCKey;
import de.sybit.hybrisgen.webauthn.data.FidoU2fAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.MakeCredentialsResponse;
import de.sybit.hybrisgen.webauthn.data.PackedAttestationStatement;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.services.AttestationStatementVerificationService;
import de.sybit.utils.AlgorithmIdentifierMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class DefaultAttestationStatementVerificationService implements AttestationStatementVerificationService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultAttestationStatementVerificationService.class);

    private static final String DIRECT_ATTESTATION = "direct";
    private static final String X_509_CERTIFICATE = "X.509";
    private static final String ATTESTATION_OBJECT_FIELD = "response.attestationObject";
    private static final String WRONG_PUBLIC_KEY_TYPE_ERROR_CODE = "public.key.wrong.type.error";
    private static final String NO_VALID_CERTIFICATE_ERROR_CODE = "no.valid.certificate.error";
    private static final String NO_VALID_STATEMENT_TYPE_ERROR_CODE = "no.valid.statement.error";
    private static final String NOT_SUPPORTED_NONE_ATTESTATION_STATEMENT_ERROR_CODE = "not.supported.none.attestation.statement";
    private static final String SHA_256_ALGORTIHM = "SHA256";
    private static final String SHA_256_WITH_ECDSA_ALGORTIHM = "SHA256withECDSA";
    private static final String NONE_ATTESTATION_TYPE_CODE = "attestation.type.none";
    private static final int EXPECTED_KEY_LENGTH = 32;
    private static final String WEBAUTHN_ATTESTATION_TYPE_CODE = "webauthn.attestation.type";

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;

    @Override
    public void verifyFidoU2fAttestationStatement(final FidoU2fAttestationStatement attestationStatement, final PublicKeyCredential credential, final Errors errorObj) {
        final CollectedClientData clientData = credential.getResponse().getClientData();
        final AttestationObject attObj = ((MakeCredentialsResponse) credential.getResponse()).getAttestationObject();
        final AuthenticatorData authData = attObj.getAuthData();
        final AttestationData attData = authData.getAttData();

        CredentialPublicKey credentialPublicKey = attData.getPublicKey();
        if (!(credentialPublicKey instanceof ECCKey)) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, WRONG_PUBLIC_KEY_TYPE_ERROR_CODE, "For FidoU2fAttestationStatement, a ECCKey is expected.");
            return;
        }

        ECCKey eccKey = (ECCKey) credentialPublicKey;
        if (eccKey.getX().length != EXPECTED_KEY_LENGTH) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, WRONG_PUBLIC_KEY_TYPE_ERROR_CODE, "The x-coordinate of the ecckey has an invalid length of bytes");
        }

        if (eccKey.getY().length != EXPECTED_KEY_LENGTH) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, WRONG_PUBLIC_KEY_TYPE_ERROR_CODE, "The y-coordinate of the ecckey has an invalid length of bytes");
        }

        try {
            DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(attestationStatement.getAttestnCert()));
            X509Certificate attestationCertificate = (X509Certificate) CertificateFactory.getInstance(X_509_CERTIFICATE).generateCertificate(inputStream);
            final MessageDigest sha256Digset = MessageDigest.getInstance(SHA_256_ALGORTIHM);
            byte[] signedBytes = Bytes.concat(new byte[]{0}, authData.getRpIdHash(), sha256Digset.digest(clientData.getClientDataBytes()), credential.getRawId(), new byte[]{0x04}, eccKey.getX(), eccKey.getY());
            if (!verifySignature(attestationCertificate.getPublicKey(), signedBytes, attestationStatement.getSig(), SHA_256_WITH_ECDSA_ALGORTIHM)) {
                errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NO_VALID_CERTIFICATE_ERROR_CODE, "Could not verify the public key with the attestation statement: " + attestationStatement.getType());
            }
        } catch (CertificateException | NoSuchAlgorithmException e) {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NO_VALID_CERTIFICATE_ERROR_CODE, "Could not verify the public key with the attestation statement");
        }

    }

    @Override
    public void verifyPackedAttestationStatement(PackedAttestationStatement attestationStatement, PublicKeyCredential credential, Errors errorObj) {
        byte[] signedBytes = null;
        try {
            MessageDigest sha256Digset = MessageDigest.getInstance(SHA_256_ALGORTIHM);
            final byte[] clientDataHash = sha256Digset.digest(credential.getResponse().getClientData().getClientDataBytes());
            signedBytes = Bytes.concat(((MakeCredentialsResponse) credential.getResponse()).getAttestationObject().getAuthData().getAuthDataBytes(), clientDataHash);
        } catch (NoSuchAlgorithmException e) {
            LOG.debug("Could not hash the clientDataBytes.");
        }

        String signatureAlgorithm;
        try {
            signatureAlgorithm = AlgorithmIdentifierMapper.get(((MakeCredentialsResponse) credential.getResponse()).getAttestationObject().getAuthData().getAttData().getPublicKey().getAlg()).getJavaAlgorithm();
        } catch (Exception e) {
            LOG.debug("Cannot find algorithm for COSE code: " + ((MakeCredentialsResponse) credential.getResponse()).getAttestationObject().getAuthData().getAttData().getPublicKey().getAlg() + "\nUse default algorithm: " + SHA_256_WITH_ECDSA_ALGORTIHM);
            signatureAlgorithm = SHA_256_WITH_ECDSA_ALGORTIHM;
        }

        if (attestationStatement.getAttestnCert() != null && attestationStatement.getCaCert() != null || attestationStatement.getEcdaaKeyId() != null) {
            try {
                DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(attestationStatement.getAttestnCert()));
                X509Certificate attestationCertificate = (X509Certificate) CertificateFactory.getInstance(X_509_CERTIFICATE).generateCertificate(inputStream);
                if (!verifySignature(attestationCertificate.getPublicKey(), signedBytes, attestationStatement.getSig(), signatureAlgorithm)) {
                    errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NO_VALID_CERTIFICATE_ERROR_CODE, "Could not verify the public key with the attestationStatement: " + attestationStatement.getType());
                }
            } catch (CertificateException e) {
                errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NO_VALID_CERTIFICATE_ERROR_CODE, "Could not verify the public key with the attestation statement");
            }
        }
    }

    @Override
    public void verifyNoneAttestationStatement(final AttestationStatement attestationStatement, final PublicKeyCredential credential, final Errors errorObj) {
        if (!attestationStatement.getType().equals(configurationService.getConfiguration().getString(NONE_ATTESTATION_TYPE_CODE, "NONE ATTESTATION"))) {
            if (!configurationService.getConfiguration().getString(WEBAUTHN_ATTESTATION_TYPE_CODE).equals(DIRECT_ATTESTATION)) {
                errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NOT_SUPPORTED_NONE_ATTESTATION_STATEMENT_ERROR_CODE, "'NONE' attestation statement is not provided when type is set to 'direct'");
            }
        } else {
            errorObj.rejectValue(ATTESTATION_OBJECT_FIELD, NO_VALID_STATEMENT_TYPE_ERROR_CODE, "The attestation statement is in an unsupported type");
        }
    }

    @Override
    public boolean verifySignature(final PublicKey pubKey, final byte[] signedBytes, final byte[] signature, final String signatureAlgorithm) {
        try {
            final Signature sig = Signature.getInstance(signatureAlgorithm);
            sig.initVerify(pubKey);
            sig.update(signedBytes);
            return sig.verify(signature);
        } catch (SignatureException | InvalidKeyException e) {
            LOG.debug("Could not verify signature of the public key.");
        } catch (NoSuchAlgorithmException e) {
            LOG.debug("Could not find SHA256 Algorithm");
        }
        return false;
    }
}
