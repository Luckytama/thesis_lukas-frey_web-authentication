package de.sybit.services.impl;

import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.model.ByteString;
import co.nstant.in.cbor.model.DataItem;
import co.nstant.in.cbor.model.Map;
import co.nstant.in.cbor.model.NegativeInteger;
import co.nstant.in.cbor.model.UnicodeString;
import co.nstant.in.cbor.model.UnsignedInteger;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import de.sybit.hybrisgen.webauthn.data.AttestationData;
import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.AttestationStatement;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.CredentialPublicKey;
import de.sybit.hybrisgen.webauthn.data.ECCKey;
import de.sybit.hybrisgen.webauthn.data.RSAKey;
import de.sybit.services.PublicKeyCredentialDecodeService;
import de.sybit.utils.AlgorithmIdentifierMapper;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.jose4j.jws.EcdsaUsingShaAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.io.InvalidObjectException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.ECParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.List;

public class DefaultPublicKeyCredentialDecodeService implements PublicKeyCredentialDecodeService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultPublicKeyCredentialDecodeService.class);

    private static final int CRV_LABEL = -1;
    private static final int X_LABEL = -2;
    private static final int Y_LABEL = -3;
    private static final int N_LABEL = -1;
    private static final int E_LABEL = -2;
    private static final int KTY_LABEL = 1;
    private static final int ALG_LABEL = 3;
    private static final String FIDO_U_2_F = "fido-u2f";
    private static final String ANDROID_SAFETYNET = "android-safetynet";
    private static final String PACKED = "packed";

    @Resource(name = "attestationStatementDecodeService")
    private DefaultAttestationStatementDecodeService attestationStatementDecodeService;

    @Override
    public AttestationObject decodeAttestationObject(byte[] attestationObject) throws InvalidObjectException {
        final AttestationObject decodedAttestationObject = new AttestationObject();
        List<DataItem> dataItems = null;
        try {
            dataItems = CborDecoder.decode(attestationObject);
        } catch (CborException e) {
            LOG.debug("Could not encode attestation object");
            throw new InvalidObjectException("Could not execute CborDecoder for attestationObject");
        }

        if (dataItems.size() == 1 && dataItems.get(0) instanceof Map) {
            Map attObjMap = (Map) dataItems.get(0);
            DataItem attStmt = null;
            for (DataItem key : attObjMap.getKeys()) {
                if (key instanceof UnicodeString) {
                    switch (((UnicodeString) key).getString()) {
                        case "fmt":
                            UnicodeString value = (UnicodeString) attObjMap.get(key);
                            decodedAttestationObject.setFmt(value.getString());
                            break;
                        case "authData":
                            final byte[] authData = ((ByteString) attObjMap.get(key)).getBytes();
                            decodedAttestationObject.setAuthData(decodeAuthData(authData));
                            break;
                        case "attStmt":
                            attStmt = attObjMap.get(key);
                            break;
                        default:
                            LOG.debug("Attestation object contains unknown key: " + ((UnicodeString) key).getString());
                            break;
                    }
                }
            }

            if (attStmt != null) {
                decodedAttestationObject.setAttStmt(decodeAttestationStatement(decodedAttestationObject.getFmt(), attStmt));
            }
        }
        return decodedAttestationObject;
    }

    @Override
    public CollectedClientData collectClientData(final byte[] clientDataBytes) throws InvalidObjectException {
        final String clientDataJson = new String(clientDataBytes, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        try {
            final CollectedClientData collectedClientData = gson.fromJson(clientDataJson, CollectedClientData.class);
            collectedClientData.setOrigin(getHostFromOrigin(collectedClientData.getOrigin()));
            collectedClientData.setClientDataBytes(clientDataBytes);
            return collectedClientData;
        } catch (JsonSyntaxException e) {
            LOG.debug("client data json does not match the excepted structure: " + clientDataJson);
            throw new InvalidObjectException("Could not parse the client Data");
        }
    }

    @Override
    public AuthenticatorData decodeAuthData(final byte[] authData) throws InvalidObjectException {
        if (authData.length < 37) {
            throw new InvalidObjectException("AuthData is not valid");
        }

        AuthenticatorData authenticatorData = new AuthenticatorData();
        authenticatorData.setAuthDataBytes(authData);

        int index = 0;
        byte[] rpIdHash = new byte[32];
        System.arraycopy(authData, 0, rpIdHash, 0, 32);
        authenticatorData.setRpIdHash(rpIdHash);

        index += 32;
        byte flags = authData[index++];
        authenticatorData.setFlags(flags);
        authenticatorData.setSignCount(Ints.fromBytes(authData[index++], authData[index++], authData[index++], authData[index++]));

        if (((flags & 0xff) & 1 << 6) != 0) {
            byte[] remainder = new byte[authData.length - index];
            System.arraycopy(authData, index, remainder, 0, authData.length - index);
            authenticatorData.setAttData(decodeAttestationData(remainder));
        }

        // Bit 7 determines whether extensions are included.
        if (((flags & 0xff) & 1 << 7) != 0) {
            int start = authData.length - index;
            if (authData.length > start) {
                byte[] remainder = new byte[authData.length - start];
                System.arraycopy(authData, start, remainder, 0, authData.length - start);
                authenticatorData.setExtensions(remainder);
            }
        }
        return authenticatorData;
    }

    @Override
    public PublicKey decodeStoredPublicKey(final byte[] publicKeyBytes) {
        try {
            CredentialPublicKey publicKey = decodePublicKey(publicKeyBytes);
            if (publicKey instanceof ECCKey) {
                return getECPublicKey((ECCKey) publicKey);
            } else if (publicKey instanceof RSAKey) {
                return getRSAPublicKey((RSAKey) publicKey);
            }
        } catch (InvalidObjectException e) {
            LOG.debug("Could not decode stored public key", e);
        }
        return null;
    }

    private String getHostFromOrigin(final String origin) {
        try {
            final URI originUri = new URI(origin);
            return originUri.getHost();
        } catch (URISyntaxException e) {
            LOG.debug("Cannot trim host from origin: " + origin + "\nNested Exception is: " + e);
        }
        return origin;
    }

    private AttestationData decodeAttestationData(final byte[] attData) throws InvalidObjectException {
        if (attData.length < 18) {
            LOG.debug("Attestation Data does not have the expected amount of bytes");
            throw new InvalidObjectException("Invalid input");
        }

        AttestationData result = new AttestationData();
        int index = 0;

        final byte[] aaguid = new byte[16];
        System.arraycopy(attData, 0, aaguid, 0, 16);
        result.setAaguid(aaguid);
        index += 16;

        int length = (attData[index++] << 8) & 0xFF;
        length += attData[index++] & 0xFF;

        final byte[] credentialId = new byte[length];
        System.arraycopy(attData, index, credentialId, 0, length);
        result.setCredentialId(credentialId);
        index += length;

        final byte[] publicKeyBytes = new byte[attData.length - index];
        System.arraycopy(attData, index, publicKeyBytes, 0, attData.length - index);
        result.setPublicKeyBytes(publicKeyBytes);
        result.setPublicKey(decodePublicKey(publicKeyBytes));

        return result;
    }

    private CredentialPublicKey decodePublicKey(final byte[] publicKey) throws InvalidObjectException {
        List<DataItem> dataItems = null;
        try {
            dataItems = CborDecoder.decode(publicKey);
        } catch (CborException e) {
            throw new InvalidObjectException("Could not encode publicKey");
        }

        if (dataItems.isEmpty() || !(dataItems.get(0) instanceof Map)) {
            throw new InvalidObjectException("The public key contains an unexpected format.");
        }

        Map map = (Map) dataItems.get(0);
        CredentialPublicKey credentialPublicKey = null;
        if (map.getKeys().size() == 4) {
            credentialPublicKey = extractRsaKey(map);
        } else {
            credentialPublicKey = extractEccKey(map);
        }
        addKtyAndAlg(map, credentialPublicKey);

        return credentialPublicKey;
    }

    private void addKtyAndAlg(final Map dataItems, final CredentialPublicKey pubKey) {
        for (DataItem item : dataItems.getKeys()) {
            int tmp = 0;
            if (item instanceof NegativeInteger) {
                tmp = ((NegativeInteger) item).getValue().intValue();
            } else if (item instanceof UnsignedInteger) {
                tmp = ((UnsignedInteger) item).getValue().intValue();
            }

            if (tmp == KTY_LABEL && dataItems.get(item) instanceof UnsignedInteger) {
                pubKey.setKty(((UnsignedInteger) dataItems.get(item)).getValue().intValue());
            } else if (tmp == ALG_LABEL && dataItems.get(item) instanceof NegativeInteger) {
                pubKey.setAlg((((NegativeInteger) dataItems.get(item)).getValue().intValue()));
            }
        }
    }

    private RSAKey extractRsaKey(final Map dataItems) {
        RSAKey rsaKey = new RSAKey();
        for (DataItem item : dataItems.getKeys()) {
            int tmp = 0;
            if (item instanceof NegativeInteger) {
                tmp = ((NegativeInteger) item).getValue().intValue();
            } else if (item instanceof UnsignedInteger) {
                tmp = ((UnsignedInteger) item).getValue().intValue();
            }

            if (tmp == N_LABEL && dataItems.get(item) instanceof ByteString) {
                rsaKey.setN(((ByteString) dataItems.get(item)).getBytes());
            } else if (tmp == E_LABEL && dataItems.get(item) instanceof ByteString) {
                rsaKey.setE(((ByteString) dataItems.get(item)).getBytes());
            }
        }
        return rsaKey;
    }

    private ECCKey extractEccKey(final Map dataItems) {
        ECCKey eccKey = new ECCKey();
        for (DataItem item : dataItems.getKeys()) {
            int labelCode = 0;
            if (item instanceof NegativeInteger) {
                labelCode = ((NegativeInteger) item).getValue().intValue();
            } else if (item instanceof UnsignedInteger) {
                labelCode = ((UnsignedInteger) item).getValue().intValue();
            }

            switch (labelCode) {
                case CRV_LABEL:
                    if (dataItems.get(item) instanceof UnsignedInteger) {
                        eccKey.setCrv(((UnsignedInteger) dataItems.get(item)).getValue().intValue());
                    }
                    break;
                case X_LABEL:
                    if (dataItems.get(item) instanceof ByteString) {
                        eccKey.setX(((ByteString) dataItems.get(item)).getBytes());
                    }
                    break;
                case Y_LABEL:
                    if (dataItems.get(item) instanceof ByteString) {
                        eccKey.setY(((ByteString) dataItems.get(item)).getBytes());
                    }
                    break;
                default:
                    LOG.debug("Unexpected field while parsing ECCKey: " + labelCode);
            }
        }
        return eccKey;
    }

    private AttestationStatement decodeAttestationStatement(final String fmt, final DataItem attStmt) throws InvalidObjectException {
        AttestationStatement attestationStatement = null;
        switch (fmt) {
            case FIDO_U_2_F:
                attestationStatement = attestationStatementDecodeService.encodeFidoU2fAttestationStatement(attStmt);
                break;
            case PACKED:
                attestationStatement = attestationStatementDecodeService.encodePackedAttestationStatement(attStmt);
                break;
            default:
                attestationStatement = attestationStatementDecodeService.encodeNoneAttestationStatement();
        }
        return attestationStatement;
    }

    private PublicKey getECPublicKey(final ECCKey eccKey) throws InvalidObjectException {
        BigInteger x = new BigInteger(eccKey.getX());
        BigInteger y = new BigInteger(eccKey.getY());
        java.security.spec.ECPoint w = new java.security.spec.ECPoint(x, y);
        EcdsaUsingShaAlgorithm algorithm = (EcdsaUsingShaAlgorithm) AlgorithmIdentifierMapper.get(eccKey.getAlg());
        String curveName = algorithm.getCurveName();
        try {
            ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(curveName);
            ECParameterSpec params = new ECNamedCurveSpec(parameterSpec.getName(), parameterSpec.getCurve(), parameterSpec.getG(), parameterSpec.getN(), parameterSpec.getH(), parameterSpec.getSeed());
            KeySpec keySpec = new java.security.spec.ECPublicKeySpec(w, params);
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            return keyFactory.generatePublic(keySpec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            throw new InvalidObjectException("Error when generate EC public key");
        }
    }

    private PublicKey getRSAPublicKey(final RSAKey rsaKey) throws InvalidObjectException {
        BigInteger modulus = new BigInteger(Bytes.concat(new byte[] {0}, rsaKey.getN()));
        BigInteger publicExponent = new BigInteger(rsaKey.getE());
        try {
            KeySpec keySpec = new RSAPublicKeySpec(modulus, publicExponent);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            throw new InvalidObjectException("Error when generate RSA public key");
        }
    }
}
