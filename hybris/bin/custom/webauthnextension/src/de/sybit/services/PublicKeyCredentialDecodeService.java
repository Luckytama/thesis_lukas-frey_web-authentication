package de.sybit.services;

import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.CredentialPublicKey;

import java.io.InvalidObjectException;
import java.security.PublicKey;

public interface PublicKeyCredentialDecodeService {

    AttestationObject decodeAttestationObject(byte[] attestationObject) throws InvalidObjectException;

    CollectedClientData collectClientData(byte[] clientDataBytes) throws InvalidObjectException;

    AuthenticatorData decodeAuthData(byte[] authData) throws InvalidObjectException;

    PublicKey decodeStoredPublicKey(byte[] publicKeyBytes);
}
