package de.sybit.converters;

import com.google.common.io.BaseEncoding;
import de.sybit.hybrisgen.webauthn.data.AuthenticateCredentialResponse;
import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.GetAssertionResponse;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.services.PublicKeyCredentialDecodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import javax.annotation.Resource;
import java.io.InvalidObjectException;

public class GetAssertionResponseConverter implements Converter<AuthenticationResponseData, PublicKeyCredential> {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticatorResponseConverter.class);
    private static final BaseEncoding urlEncoder = BaseEncoding.base64Url().omitPadding();
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();

    @Resource(name = "publicKeyCredentialDecodeService")
    private PublicKeyCredentialDecodeService publicKeyCredentialDecodeService;

    @Override
    public PublicKeyCredential convert(AuthenticationResponseData authenticationResponseData) {
        final PublicKeyCredential publicKeyCredential = new PublicKeyCredential();

        publicKeyCredential.setId(authenticationResponseData.getId());
        publicKeyCredential.setRawId(urlEncoder.decode(authenticationResponseData.getId()));
        publicKeyCredential.setType(authenticationResponseData.getType());
        publicKeyCredential.setResponse(convertCredentialsResponse(authenticationResponseData.getResponse()));

        return publicKeyCredential;
    }

    private GetAssertionResponse convertCredentialsResponse(final AuthenticateCredentialResponse response) {

        GetAssertionResponse authenticatorAttestationResponse = new GetAssertionResponse();
        try {
            final byte[] clientDataBytes = baseEncoder.decode(response.getClientDataJSON());
            final byte[] authenticatorDataBytes = baseEncoder.decode(response.getAuthenticatorData());

            final CollectedClientData collectedClientData = publicKeyCredentialDecodeService.collectClientData(clientDataBytes);
            final AuthenticatorData authenticatorData = publicKeyCredentialDecodeService.decodeAuthData(authenticatorDataBytes);

            authenticatorAttestationResponse.setClientData(collectedClientData);
            authenticatorAttestationResponse.setAuthenticatorData(authenticatorData);
            authenticatorAttestationResponse.setSignature(baseEncoder.decode(response.getSignature()));
            authenticatorAttestationResponse.setUserHandle(baseEncoder.decode(response.getUserHandle()));
        } catch (InvalidObjectException e) {
            LOG.debug("Could not decode authenticator attestation response data");
        }
        return authenticatorAttestationResponse;
    }
}
