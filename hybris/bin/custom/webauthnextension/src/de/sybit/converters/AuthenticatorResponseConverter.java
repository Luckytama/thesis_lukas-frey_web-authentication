package de.sybit.converters;

import com.google.common.io.BaseEncoding;

import de.sybit.hybrisgen.webauthn.data.AttestationObject;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorResponseData;
import de.sybit.hybrisgen.webauthn.data.CollectedClientData;
import de.sybit.hybrisgen.webauthn.data.CredentialResponse;
import de.sybit.hybrisgen.webauthn.data.MakeCredentialsResponse;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.services.PublicKeyCredentialDecodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import javax.annotation.Resource;
import java.io.InvalidObjectException;

public class AuthenticatorResponseConverter implements Converter<AuthenticatorResponseData, PublicKeyCredential> {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticatorResponseConverter.class);
    private static final BaseEncoding urlEncoder = BaseEncoding.base64Url().omitPadding();
    private static final BaseEncoding baseEncoder = BaseEncoding.base64();

    @Resource(name = "publicKeyCredentialDecodeService")
    private PublicKeyCredentialDecodeService publicKeyCredentialDecodeService;

    @Override
    public PublicKeyCredential convert(AuthenticatorResponseData authenticatorResponseData) {
        final PublicKeyCredential publicKeyCredential = new PublicKeyCredential();

        publicKeyCredential.setId(authenticatorResponseData.getId());
        publicKeyCredential.setRawId(urlEncoder.decode(authenticatorResponseData.getId()));
        publicKeyCredential.setType(authenticatorResponseData.getType());
        publicKeyCredential.setResponse(convertMakeCredentialsResponse(authenticatorResponseData.getResponse()));

        return publicKeyCredential;
    }

    private MakeCredentialsResponse convertMakeCredentialsResponse(final CredentialResponse response) {
        MakeCredentialsResponse authenticatorAttestationResponse = new MakeCredentialsResponse();
        try {
            final byte[] clientDataBytes = baseEncoder.decode(response.getClientDataJSON());
            final byte[] attestationObject = baseEncoder.decode(response.getAttestationObject());

            final CollectedClientData collectedClientData = publicKeyCredentialDecodeService.collectClientData(clientDataBytes);
            final AttestationObject decodedAttestationObject = publicKeyCredentialDecodeService.decodeAttestationObject(attestationObject);

            authenticatorAttestationResponse.setAttestationObject(decodedAttestationObject);
            authenticatorAttestationResponse.setClientData(collectedClientData);
            authenticatorAttestationResponse.setTransports(response.getTransports());
        } catch (InvalidObjectException e) {
            LOG.debug("Could not decode authenticator attestation response data");
        }
        return authenticatorAttestationResponse;
    }
}
