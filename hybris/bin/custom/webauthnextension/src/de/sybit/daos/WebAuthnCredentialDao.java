package de.sybit.daos;

import de.hybris.platform.core.model.user.UserModel;
import de.sybit.model.WebAuthnCredentialModel;

import java.util.Optional;

public interface WebAuthnCredentialDao {
    Optional<WebAuthnCredentialModel> findCredentialByCredentialId(String credentialId);

    Optional<UserModel> findUserByCredentialId(String credentialId);
}
