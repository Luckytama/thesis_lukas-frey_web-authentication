package de.sybit.daos.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.sybit.daos.WebAuthnCredentialDao;
import de.sybit.model.WebAuthnCredentialModel;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

public class DefaultWebAuthnCredentialDao implements WebAuthnCredentialDao {

    private static final String FIND_CREDENTIALS_STRING = "SELECT {c:" + WebAuthnCredentialModel.PK + "} " + "FROM {" + WebAuthnCredentialModel._TYPECODE + " AS c} ";
    private static final String FIND_CREDENTIALS_BY_ID_STRING = FIND_CREDENTIALS_STRING + "WHERE {c:" + WebAuthnCredentialModel.CREDENTIALID + "}=?credentialId";
    private static final String FIND_USER_BY_CREDENTIAL_ID_STRING = "SELECT {u:" + UserModel.PK + "} FROM {" + UserModel._TYPECODE + " AS u JOIN " + WebAuthnCredentialModel._TYPECODE + " AS w ON {u:" + UserModel.WEBAUTHNCREDENTIALS + "} LIKE CONCAT( '%', CONCAT( {w:" + WebAuthnCredentialModel.PK + "} , '%' ) )} WHERE {w:" + WebAuthnCredentialModel.CREDENTIALID + "}=?credentialId";

    @Resource(name = "flexibleSearchService")
    private FlexibleSearchService flexibleSearchService;

    @Override
    public Optional<WebAuthnCredentialModel> findCredentialByCredentialId(final String credentialId) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_CREDENTIALS_BY_ID_STRING);
        query.addQueryParameter("credentialId", credentialId);
        List<WebAuthnCredentialModel> credentials = flexibleSearchService.<WebAuthnCredentialModel>search(query).getResult();
        return credentials.isEmpty() ? Optional.empty() : Optional.of(credentials.get(0));
    }

    @Override
    public Optional<UserModel> findUserByCredentialId(final String credentialId) {
        final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_USER_BY_CREDENTIAL_ID_STRING);
        query.addQueryParameter("credentialId", credentialId);
        List<UserModel> userModels = flexibleSearchService.<UserModel>search(query).getResult();
        return userModels.isEmpty() ? Optional.empty() : Optional.of(userModels.get(0));
    }

}
