package de.sybit.security.provider;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.sybit.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.sybit.facades.WebAuthnCredentialsFacade;
import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.security.details.WebAuthnUserDetailsService;
import de.sybit.security.token.WebAuthnAssertionAuthenticationToken;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
public class WebAuthnAuthenticationProviderTest {

    private static final String CREDENTIAL_JSON = "{\"id\":\"WoOgSvCsRqHYMOfqelpQTWV9qxQmPfWhdRfTYi3YKk36u67Xt8-2oQSuZF4Ov9vRJTb4hZd4j7Jnveuc03LjNg\",\"type\":\"public-key\",\"rawId\":\"WoOgSvCsRqHYMOfqelpQTWV9qxQmPfWhdRfTYi3YKk36u67Xt8+2oQSuZF4Ov9vRJTb4hZd4j7Jnveuc03LjNg==\",\"response\":{\"clientDataJSON\":\"eyJjaGFsbGVuZ2UiOiJSazBQM0NpR0c0Nmx3QjNCcnhxNmJKTXpMVUczZWs1dU1YeF9QZjFJU2s0Iiwib3JpZ2luIjoiaHR0cHM6Ly9lbGVjdHJvbmljcy5sb2NhbDo5MDAyIiwidHlwZSI6IndlYmF1dGhuLmdldCJ9\",\"authenticatorData\":\"kG2aXljoCs7kpxWparML1oSiGfwaQh7m3e+wv6X6GZIBAAACUQ==\",\"signature\":\"MEUCIQDh54yc70p1CAAjdknjq2YVoXKS5YxQk7rBoBaYE0z8HgIgT8k6zWdfZJVcy7Yb10WM/QIPgArV8c4F6ajRDLnv6a4=\",\"userHandle\":\"\"}}";
    private static final String CREDENTIAL_ID = "CREDENTIAL_ID";
    private static final String USERNAME = "USERNAME";
    private static final String ROLE_ADMIN_GROUP = "ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase();

    @Mock
    private WebAuthnCredentialsFacade webAuthnCredentialsFacade;

    @Mock
    private WebAuthnUserDetailsService userDetailsService;

    @Mock
    private UserService userService;

    @Mock
    private ModelService modelService;

    @Mock
    private BruteForceAttackCounter bruteForceAttackCounter;

    @Mock
    private MessageSourceAccessor messages;

    @InjectMocks
    private WebAuthnAuthenticationProvider webAuthnAuthenticationProvider;

    @Mock
    private WebAuthnAssertionAuthenticationToken assertionAuthenticationToken;

    @Mock
    private AuthenticationResponseData credentials;

    @Mock
    private PublicKeyCredential publicKeyCredential;

    @Mock
    private UserDetails userDetails;

    @Mock
    private UserModel userModel;

    @Mock
    private UserGroupModel customerUserGroup;

    @Mock
    private List authorities;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(assertionAuthenticationToken.getCredentials()).thenReturn(CREDENTIAL_JSON);
        when(webAuthnCredentialsFacade.verifyGetAssertion(any(), any())).thenReturn(publicKeyCredential);
        when(webAuthnCredentialsFacade.finishGetAssertion(publicKeyCredential)).thenReturn(CREDENTIAL_ID);
        when(userDetailsService.loadUserByCredentialId(CREDENTIAL_ID)).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn(USERNAME);
        when(userService.getUserGroupForUID(Constants.USER.CUSTOMER_USERGROUP)).thenReturn(customerUserGroup);
    }

    @Test
    public void authenticate_tokenSupported_noBruteForce_memberOfCustomerGroup_noErrors_noException() {
        when(bruteForceAttackCounter.isAttack(USERNAME)).thenReturn(false);
        when(userService.getUserForUID(USERNAME)).thenReturn(userModel);
        when(userService.isMemberOfGroup(userModel, customerUserGroup)).thenReturn(true);

        final Authentication authToken = webAuthnAuthenticationProvider.authenticate(assertionAuthenticationToken);

        verify(webAuthnCredentialsFacade).verifyGetAssertion(any(), any());
        verify(webAuthnCredentialsFacade).finishGetAssertion(publicKeyCredential);
        verify(userDetailsService).loadUserByCredentialId(CREDENTIAL_ID);
        verify(bruteForceAttackCounter).isAttack(USERNAME);
        verify(userService).getUserForUID(USERNAME);
        verify(modelService, never()).save(userModel);
        verify(bruteForceAttackCounter, never()).resetUserCounter(USERNAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void authenticate_tokenNotSupported_exception() {
        webAuthnAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(null, null));
    }

    @Test(expected = BadCredentialsException.class)
    public void authenticate_isBruteForceAttack_exception() {
        when(bruteForceAttackCounter.isAttack(USERNAME)).thenReturn(true);
        when(userService.getUserForUID(USERNAME)).thenReturn(userModel);
        when(userService.isMemberOfGroup(userModel, customerUserGroup)).thenReturn(true);

        final Authentication authToken = webAuthnAuthenticationProvider.authenticate(assertionAuthenticationToken);

        verify(webAuthnCredentialsFacade).verifyGetAssertion(any(), any());
        verify(webAuthnCredentialsFacade).finishGetAssertion(publicKeyCredential);
        verify(userDetailsService).loadUserByCredentialId(CREDENTIAL_ID);
        verify(bruteForceAttackCounter).isAttack(USERNAME);
        verify(userService).getUserForUID(USERNAME);
        verify(userModel).setLoginDisabled(true);
        verify(modelService).save(userModel);
        verify(bruteForceAttackCounter).resetUserCounter(USERNAME);
    }

    @Test(expected = BadCredentialsException.class)
    public void authenticate_isNoMemberOfCustomerGroup_exception() {
        when(bruteForceAttackCounter.isAttack(USERNAME)).thenReturn(false);
        when(userService.getUserForUID(USERNAME)).thenReturn(userModel);
        when(userService.isMemberOfGroup(userModel, customerUserGroup)).thenReturn(false);

        final Authentication authToken = webAuthnAuthenticationProvider.authenticate(assertionAuthenticationToken);

        verify(webAuthnCredentialsFacade).verifyGetAssertion(any(), any());
        verify(webAuthnCredentialsFacade).finishGetAssertion(publicKeyCredential);
        verify(userDetailsService).loadUserByCredentialId(CREDENTIAL_ID);
        verify(bruteForceAttackCounter).isAttack(USERNAME);
        verify(userService).getUserForUID(USERNAME);
        verify(modelService, never()).save(userModel);
        verify(bruteForceAttackCounter, never()).resetUserCounter(USERNAME);
    }

    @Test
    public void additionalAuthenticationChecks_noAdminAuthority_noException() {
        when(userDetails.getAuthorities()).thenReturn(Collections.emptyList());

        webAuthnAuthenticationProvider.additionalAuthenticationChecks(userDetails, null);
    }

    @Test(expected = LockedException.class)
    public void additionalAuthenticationChecks_adminAuthority_exception() {
        when(userDetails.getAuthorities()).thenReturn(authorities);
        when(authorities.contains(new SimpleGrantedAuthority(ROLE_ADMIN_GROUP))).thenReturn(true);

        webAuthnAuthenticationProvider.additionalAuthenticationChecks(userDetails, null);
    }
}