package de.sybit.security.details;

import de.hybris.platform.core.model.user.UserModel;
import de.sybit.model.WebAuthnCredentialModel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;
import java.util.List;

public class WebAuthnUserDetails extends User {

    private final List<WebAuthnCredentialModel> webAuthnCredentials;

    private static final List<? extends GrantedAuthority> DEFAULT_AUTHORITIES = Collections.singletonList(new SimpleGrantedAuthority("ROLE_CUSTOMERGROUP"));

    private static final List<? extends GrantedAuthority> WEBAUTHN_AUTHORITIES = Collections.singletonList(new SimpleGrantedAuthority("ROLE_WEBAUTHN_USER"));

    WebAuthnUserDetails(final UserModel user, final boolean hasWebAuthnCredentials) {
        super(user.getUid(), user.getEncodedPassword(), true, true, true, true, hasWebAuthnCredentials ? WEBAUTHN_AUTHORITIES : DEFAULT_AUTHORITIES);
        this.webAuthnCredentials = user.getWebAuthnCredentials();
    }

    public List<WebAuthnCredentialModel> getWebAuthnCredentials() {
        return webAuthnCredentials;
    }
}
