package de.sybit.security.filter;

import de.sybit.security.token.WebAuthnAssertionAuthenticationToken;
import org.apache.solr.common.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class WebAuthnProcessingFilter extends UsernamePasswordAuthenticationFilter {

    private static final String SPRING_SECURITY_FORM_PUBLIC_KEY_CREDENTIALS = "publicKeyCredentials";
    private static final String SPRING_SECURITY_FORM_USERNAME = "j_username";
    private static final String SPRING_SECURITY_FORM_PASSWORD = "j_password";

    private List<GrantedAuthority> authorities;
    private String publicKeyCredentialsParameter = SPRING_SECURITY_FORM_PUBLIC_KEY_CREDENTIALS;
    private String usernameParameter = SPRING_SECURITY_FORM_USERNAME;
    private String passwordParameter = SPRING_SECURITY_FORM_PASSWORD;

    @Resource(name = "authenticationManager")
    private AuthenticationManager authenticationManager;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String username = obtainUsername(request);
        String password = obtainPassword(request);

        String authJson = obtainPublicKeyCredentials(request);

        UsernamePasswordAuthenticationToken authRequest;
        if (StringUtils.isEmpty(authJson)) {
            authRequest = new UsernamePasswordAuthenticationToken(username, password, authorities);
        } else {
            authRequest = new WebAuthnAssertionAuthenticationToken(authJson);
        }

        setDetails(request, authRequest);

        return authenticationManager.authenticate(authRequest);
    }

    public void setPublicKeyCredentialsParameter(final String publicKeyCredentialsParameter) {
        this.publicKeyCredentialsParameter = publicKeyCredentialsParameter;
    }

    public String getPublicKeyCredentialsParameter() {
        return this.publicKeyCredentialsParameter;
    }

    private String obtainPublicKeyCredentials(HttpServletRequest request) {
        return request.getParameter(this.publicKeyCredentialsParameter);
    }

    @Override
    public String obtainUsername(HttpServletRequest request) {
        return request.getParameter(this.usernameParameter);
    }

    @Override
    public String obtainPassword(HttpServletRequest request) {
        return request.getParameter(this.passwordParameter);
    }
}
