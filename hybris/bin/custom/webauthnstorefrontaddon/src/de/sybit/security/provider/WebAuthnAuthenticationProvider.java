package de.sybit.security.provider;

import com.google.gson.Gson;
import de.sybit.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;
import de.hybris.platform.spring.security.CoreUserDetails;
import de.sybit.facades.WebAuthnCredentialsFacade;
import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.security.details.WebAuthnUserDetailsService;
import de.sybit.security.token.WebAuthnAssertionAuthenticationToken;
import de.sybit.security.token.WebAuthnAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import javax.annotation.Resource;
import java.util.Collections;

public class WebAuthnAuthenticationProvider extends CoreAuthenticationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(WebAuthnAuthenticationProvider.class);

    private static final String ROLE_ADMIN_GROUP = "ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase();
    private static final String CORE_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS = "CoreAuthenticationProvider.badCredentials";
    private static final String BAD_CREDENTIALS = "Bad credentials";
    private static final String CORE_AUTHENTICATION_PROVIDER_CREDENTIALS_EXPIRED = "CoreAuthenticationProvider.credentialsExpired";
    private static final String USER_CREDENTIALS_HAVE_EXPIRED = "User credentials have expired.";

    private GrantedAuthority adminAuthority = new SimpleGrantedAuthority(ROLE_ADMIN_GROUP);

    @Resource(name = "webAuthnCredentialsFacade")
    private WebAuthnCredentialsFacade webAuthnCredentialsFacade;

    @Resource(name = "webAuthnUserDetailsService")
    private WebAuthnUserDetailsService userDetailsService;

    @Resource(name = "sybitBruteForceAttackCounter")
    private BruteForceAttackCounter bruteForceAttackCounter;

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "modelService")
    private ModelService modelService;

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        if (!supports(authentication.getClass())) {
            throw new IllegalArgumentException("Only WebAuthnAssertionAuthenticationToken is supported");
        }

        final WebAuthnAssertionAuthenticationToken authenticationToken = (WebAuthnAssertionAuthenticationToken) authentication;

        final Gson g = new Gson();
        final AuthenticationResponseData credentials = g.fromJson(authenticationToken.getCredentials(), AuthenticationResponseData.class);

        final Errors errorObj = new BindException(authentication, "Authentication");

        final PublicKeyCredential webAuthnCredentials = webAuthnCredentialsFacade.verifyGetAssertion(credentials, errorObj);
        final String credentialId = webAuthnCredentialsFacade.finishGetAssertion(webAuthnCredentials);

        UserDetails userDetails = userDetailsService.loadUserByCredentialId(credentialId);
        final String username = userDetails.getUsername();
        final boolean isBruteForceAttack = bruteForceAttackCounter.isAttack(username);

        UserModel userModel = userService.getUserForUID(username);

        if (isBruteForceAttack) {
            userModel.setLoginDisabled(true);
            modelService.save(userModel);
            bruteForceAttackCounter.resetUserCounter(username);
            throw new BadCredentialsException(messages.getMessage(CORE_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, BAD_CREDENTIALS));
        }

        if (!userService.isMemberOfGroup(userModel, userService.getUserGroupForUID(Constants.USER.CUSTOMER_USERGROUP))) {
            throw new BadCredentialsException(messages.getMessage(CORE_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, BAD_CREDENTIALS));
        }

        if (errorObj.hasErrors()) {
            throw new BadCredentialsException(messages.getMessage(CORE_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, BAD_CREDENTIALS));
        }

        if (Registry.hasCurrentTenant() && JaloConnection.getInstance().isSystemInitialized()) {
            this.getPreAuthenticationChecks().check(userDetails);
            this.additionalAuthenticationChecks(userDetails, (AbstractAuthenticationToken) authentication);

            if (!userDetails.isCredentialsNonExpired()) {
                throw new CredentialsExpiredException(messages.getMessage(CORE_AUTHENTICATION_PROVIDER_CREDENTIALS_EXPIRED, USER_CREDENTIALS_HAVE_EXPIRED));
            }
            return new WebAuthnAuthenticationToken(username, credentials, userDetails.getAuthorities());
        } else {
            return this.createSuccessAuthentication(authentication, new CoreUserDetails("systemNotInitialized", "systemNotInitialized", true, false, true, true, Collections.emptyList(), (String) null));
        }
    }

    @Override
    protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
            throws AuthenticationException {
        super.additionalAuthenticationChecks(details, authentication);

        // Check if the user is in role admingroup
        if (adminAuthority != null && details.getAuthorities().contains(adminAuthority)) {
            throw new LockedException("Login attempt as " + Constants.USER.ADMIN_USERGROUP + " is rejected");
        }
    }

    @Override
    public boolean supports(Class aClass) {
        return WebAuthnAssertionAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
