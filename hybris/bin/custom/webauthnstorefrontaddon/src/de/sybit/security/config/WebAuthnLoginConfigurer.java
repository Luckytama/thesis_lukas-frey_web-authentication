package de.sybit.security.config;

import de.sybit.security.filter.WebAuthnProcessingFilter;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class WebAuthnLoginConfigurer<H extends HttpSecurityBuilder<H>> extends AbstractAuthenticationFilterConfigurer<H, WebAuthnLoginConfigurer<H>, WebAuthnProcessingFilter> {

    private String usernameParameter = null;
    private String passwordParameter = null;
    private String publicKeyCredentialsParameter = null;

    public WebAuthnLoginConfigurer() {
        super(new WebAuthnProcessingFilter(), null);
    }

    @Override
    public void configure(H http) throws Exception {
        super.configure(http);
        if (usernameParameter != null) {
            this.getAuthenticationFilter().setUsernameParameter(usernameParameter);
        }

        if (passwordParameter != null) {
            this.getAuthenticationFilter().setPasswordParameter(passwordParameter);
        }

        if (publicKeyCredentialsParameter != null) {
            this.getAuthenticationFilter().setPublicKeyCredentialsParameter(publicKeyCredentialsParameter);
        }
    }

    @Override
    protected RequestMatcher createLoginProcessingUrlMatcher(String s) {
        return null;
    }
}
