package de.sybit.security.token;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class WebAuthnAssertionAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private final String publicKeyCredentials;

    public WebAuthnAssertionAuthenticationToken(String publicKeyCredentials) {
        super(null, null);
        this.publicKeyCredentials = publicKeyCredentials;
    }

    public WebAuthnAssertionAuthenticationToken(Object principal, Object credentials, String publicKeyCredentials) {
        super(principal, credentials);
        this.publicKeyCredentials = publicKeyCredentials;
    }

    public WebAuthnAssertionAuthenticationToken(Object principal, Object credentials, String publicKeyCredentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.publicKeyCredentials = publicKeyCredentials;
    }

    @Override
    public String getCredentials() {
        return publicKeyCredentials;
    }
}
