package de.sybit.security.handler;

import de.sybit.acceleratorstorefrontcommons.security.StorefrontAuthenticationSuccessHandler;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebAuthnLoginSuccessHandler extends StorefrontAuthenticationSuccessHandler {

    private static final Logger LOG = Logger.getLogger(WebAuthnLoginSuccessHandler.class);

    private static final GrantedAuthority WEBAUTHN_USER_ROLE = new SimpleGrantedAuthority("ROLE_WEBAUTHN_USER");
    private static final String LOGIN_WEBAUTHN_URL = "/login/webauthn";

    @Override
    public String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response) {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getAuthorities().contains(WEBAUTHN_USER_ROLE)) {
            return LOGIN_WEBAUTHN_URL;
        } else {
            return super.determineTargetUrl(request, response);
        }
    }
}
