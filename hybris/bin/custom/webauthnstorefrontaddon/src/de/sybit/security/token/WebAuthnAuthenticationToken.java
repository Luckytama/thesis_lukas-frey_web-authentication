package de.sybit.security.token;

import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;

public class WebAuthnAuthenticationToken extends AbstractAuthenticationToken {

    private Serializable principal;
    private AuthenticationResponseData credentials;

    public WebAuthnAuthenticationToken(final Serializable principal, final AuthenticationResponseData credentials, final Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        this.setAuthenticated(true);
    }

    @Override
    public AuthenticationResponseData getCredentials() {
        return credentials;
    }

    @Override
    public Serializable getPrincipal() {
        return principal;
    }
}
