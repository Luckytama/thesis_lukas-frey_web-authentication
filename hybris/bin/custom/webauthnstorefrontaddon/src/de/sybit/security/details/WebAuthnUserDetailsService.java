package de.sybit.security.details;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.sybit.daos.WebAuthnCredentialDao;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.util.Optional;

public class WebAuthnUserDetailsService implements UserDetailsService {

    @Resource(name = "userService")
    private UserService userService;

    @Resource(name = "webAuthnCredentialDao")
    private WebAuthnCredentialDao credentialDao;

    @Override
    public UserDetails loadUserByUsername(final String userUID) throws UsernameNotFoundException {
        UserModel userModel = userService.getUserForUID(userUID);
        if (userModel.getWebAuthnCredentials().isEmpty()) {
            return new WebAuthnUserDetails(userModel, false);
        } else {
            return new WebAuthnUserDetails(userModel, true);
        }
    }

    public UserDetails loadUserByCredentialId(final String credentialId) {
        Optional<UserModel> userModel = credentialDao.findUserByCredentialId(credentialId);
        if (userModel.isEmpty()) {
            return null;
        } else {
            return new WebAuthnUserDetails(userModel.get(), false);
        }
    }
}
