var WebAuthn = WebAuthn || {};
WebAuthn.utils = {

    binToStr: function (bin) {
        return btoa(new Uint8Array(bin).reduce((s, byte) => s + String.fromCharCode(byte), ''));
    },

    strToBin: function (str) {
        return Uint8Array.from(atob(str), c => c.charCodeAt(0));
    },

    base64UrlToMime: function(code) {
        return code.replace(/-/g, '+').replace(/_/g, '/') + '===='.substring(0, (4 - (code.length % 4)) % 4);
    }
};