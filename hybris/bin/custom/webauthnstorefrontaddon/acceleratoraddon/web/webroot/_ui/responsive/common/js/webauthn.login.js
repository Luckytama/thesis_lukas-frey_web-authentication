var WebAuthn = WebAuthn || {};
WebAuthn.login = {

    bindAll: function () {
        WebAuthn.login.bindStaticOnClickHandlers();
    },

    bindStaticOnClickHandlers: function () {
        $(".webauthn-login-btn").on("click keypress", function() {
           WebAuthn.login.createAssertionOptions();
        });
    },

    createAssertionOptions: function () {
        var url = $(".webauthn-login-btn").data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (assertionOptions) {
                if (assertionOptions != null) {
                    if ($(".webauthn-login-btn").data("action") === "login") {
                        WebAuthn.login.doLogin(assertionOptions);
                    } else {
                        WebAuthn.login.doAuthenticate(assertionOptions)
                    }
                }
            },
            error: function () {
                console.log("There went something wrong while creating WebAuthn Credentials");
            }
        })
    },

    doAuthenticate: function (publicKeyCredentialRequestOptions) {
        WebAuthn.login.decodeAssertionOptions(publicKeyCredentialRequestOptions);
        navigator.credentials.get({
            "publicKey": publicKeyCredentialRequestOptions
        }).then(function (credential) {
            var publicKeyCredential = WebAuthn.login.decodeAuthenticationData(credential);
            var url = $(".webauthn-login").data("url");
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(publicKeyCredential),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (pubKey) {
                    var authenticatedCredential = $(".credentials").find("[data-id=" + pubKey + "]");
                    authenticatedCredential.addClass("selected");
                    setTimeout(function () {
                        authenticatedCredential.removeClass("selected")
                    }, 3000);
                },
                error: function (error) {
                    $.colorbox({open: true, html: error.responseText});
                }
            });
        });
    },

    doLogin: function (assertionOptions) {
        WebAuthn.login.decodeAssertionOptions(assertionOptions);
        navigator.credentials.get({
            "publicKey": assertionOptions
        }).then(function (credential) {
            var publicKeyCredential = WebAuthn.login.decodeAuthenticationData(credential);
            var url = $(".webauthn-login").data("url");
            $.ajax({
                type: "POST",
                url: url,
                data: {publicKeyCredentials: JSON.stringify(publicKeyCredential)},
                success: function (data) {
                    window.location.href = document.referrer;
                }
            });
        });
    },

    decodeAssertionOptions: function(assertionOptions) {
        assertionOptions.challenge = WebAuthn.utils.strToBin(assertionOptions.challenge);
        assertionOptions.allowCredentials.forEach(function (credential) {
            credential.id = WebAuthn.utils.strToBin(WebAuthn.utils.base64UrlToMime(credential.id));
        });
    },

    decodeAuthenticationData: function (assertion) {
        var publicKeyCredential = {};

        if ('id' in assertion) {
            publicKeyCredential.id = assertion.id;
        }
        if ('type' in assertion) {
            publicKeyCredential.type = assertion.type;
        }
        if ('rawId' in assertion) {
            publicKeyCredential.rawId = WebAuthn.utils.binToStr(assertion.rawId);
        }
        if (!assertion.response) {
            throw "Get assertion response lacking 'response' attribute";
        }
        if (assertion.getClientExtensionResults) {
            if (assertion.getClientExtensionResults().uvm != null) {
                publicKeyCredential.uvm = serializeUvm(assertion.getClientExtensionResults().uvm);
            }
        }

        var _response = assertion.response;

        publicKeyCredential.response = {
            clientDataJSON: WebAuthn.utils.binToStr(_response.clientDataJSON),
            authenticatorData: WebAuthn.utils.binToStr(_response.authenticatorData),
            signature: WebAuthn.utils.binToStr(_response.signature),
            userHandle: WebAuthn.utils.binToStr(_response.userHandle)
        };

        return publicKeyCredential;
    }
};

$(document).ready(function () {
    WebAuthn.login.bindAll()
});