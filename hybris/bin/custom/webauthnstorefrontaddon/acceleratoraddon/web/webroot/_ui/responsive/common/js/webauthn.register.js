var WebAuthn = WebAuthn || {};
WebAuthn.register = {

    createOptions : {},
    credential : {},
    decodedCredential : {},

    bindAll : function () {
        WebAuthn.register.bindStaticOnClickHandlers();
        WebAuthn.register.deleteCredential();
    },

    bindStaticOnClickHandlers : function () {
        $(".webauthn-register-credentials-btn").on("click keypress", function() {
            WebAuthn.register.createCredentialCreateOptions();
        });
    },

    doRegister : function () {
        navigator.credentials.create({
            "publicKey": WebAuthn.register.createOptions
        }).then(function (credential) {
            WebAuthn.register.saveCredentials(credential)
        });
    },

    saveCredentials : function (credential) {
        var publicKeyCredential = WebAuthn.register.decodeAuthenticationData(credential);
        var url = $(".post_credential_url").data("url");
        $.ajax({
            type: "POST",
            url: url,
            contentType : 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify(publicKeyCredential),
            success: function (response) {
                $.colorbox({open: true, html: response});
            },
            error: function (response) {
                $.colorbox({open: true, html: response.responseText});
            }
        });
    },

    decodeAuthenticationData : function (authenticationData) {
        var publicKeyCredential = {};
        if ('id' in authenticationData) {
            publicKeyCredential.id = authenticationData.id;
        }

        if ('type' in authenticationData) {
            publicKeyCredential.type = authenticationData.type;
        }

        if ('rawId' in authenticationData) {
            publicKeyCredential.rawId = WebAuthn.utils.binToStr(authenticationData.rawId);
        }

        if (!authenticationData.response) {
            console.log("Authentication Data has no attribute named 'response'. Please try it again.")
        }

        var response = {};
        response.clientDataJSON = WebAuthn.utils.binToStr(authenticationData.response.clientDataJSON);
        response.attestationObject = WebAuthn.utils.binToStr(authenticationData.response.attestationObject);

        if (authenticationData.response.getClientExtensionResults) {
            publicKeyCredential.extensions = attestation.getClientExtensionResults();
            if (authenticationData.getClientExtensionResults().uvm != null) {
                publicKeyCredential.uvm = WebAuthn.register.serializeUvm(authenticationData.getClientExtensionResults().uvm);
            }
        }

        if (authenticationData.response.getTransports) {
            response.transports = authenticationData.response.getTransports();
        }

        publicKeyCredential.response = response;
        return publicKeyCredential;
    },

    createCredentialCreateOptions : function () {
        WebAuthn.register.getCredentialCreateOptions();
    },

    getCredentialCreateOptions : function () {
        var url = $(".webauthn-register-credentials-btn").data("url");
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                WebAuthn.register.storeCreateOptions(result);
                WebAuthn.register.doRegister();
            },
            error: function () {
                console.log("There went something wrong while creating WebAuthn Credentials");
            }
        })
    },

    storeCreateOptions : function (options) {
        WebAuthn.register.createOptions = options;
        WebAuthn.register.createOptions.user.id = Uint8Array.from(atob(options.user.id), c => c.charCodeAt(0));
        WebAuthn.register.createOptions.challenge = Uint8Array.from(atob(options.challenge), c => c.charCodeAt(0));
    },

    serializeUvm : function (uvm) {
        var uvmJson = [];
        for (var i = 0; i < uvm.length; i++) {
            var uvmEntry = {};
            uvmEntry.userVerificationMethod = uvm[i][0];
            uvmEntry.keyProtectionType = uvm[i][1];
            uvmEntry.atchuvmJsonerProtectionType = uvm[i][2];
            uvmJson.push(uvmEntry);
        }
        return uvmJson;
    },
    
    deleteCredential : function () {
        $(".delete-icon").click( function () {
            var credentialId = $(this).data("id").trim();
            var url = $(this).data("url");
            $.ajax({
                type: "POST",
                url: url,
                data: {credentialId: credentialId},
                global: false,
                async: false,
                success: function () {
                    location.reload();
                },
                error: function () {
                    console.log("Something went wrong while deleting credentials ")
                }
            })
        })
    }
};

$(document).ready(function () {
    WebAuthn.register.bindAll()
});