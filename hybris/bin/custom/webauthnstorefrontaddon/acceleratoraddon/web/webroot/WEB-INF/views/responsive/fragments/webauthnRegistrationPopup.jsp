<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:url var="saveCredentialUrl" value="/my-account/webauthn/save-credential"/>
<div class="popUpContainer">
    <div class="popUp-header">
        <div class="row">
            <h3><spring:theme code="webauthn.popup.registration.title"/></h3>
        </div>
    </div>
    <div class="col-lg-12 popUp-content">
        <p><spring:theme code="${popupContent}"/></p>
        <div class="row">
            <form:form id="registration-title-form" method="POST" action="${saveCredentialUrl}">
                <input type="text" name="title" required/>
                <input type="submit" value="Submit"/>
            </form:form>
        </div>
    </div>
</div>