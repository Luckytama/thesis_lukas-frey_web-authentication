<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="webauthn" tagdir="/WEB-INF/tags/addons/webauthnstorefrontaddon/webauthn" %>

<c:url value="/my-account/webauthn/verify-credential" var="verifyCredentialUrl"/>
<c:url value="/my-account/webauthn/delete-credential" var="deleteCredentialUrl"/>
<c:url value="/my-account/webauthn/create-options" var="createOptionsUrl"/>
<c:url value="/my-account/webauthn/authenticate" var="authenticateUrl"/>
<c:url value="/my-account/webauthn/verify-assertion" var="verifyAssertionUrl"/>

<div class="account-section-header">
    <spring:theme code="webauthn.page.header"/>
</div>
<c:if test="${not empty credentials}">
    <div class="description">
        <spring:theme code="webauthn.page.description"/>
    </div>
    <div class="credentials row">
        <c:forEach items="${credentials}" var="credential">
            <div class="column">
                <div class="credential" data-id="${credential.credentialID}">
                    <div class="credential-title h4">
                        <spring:htmlEscape defaultHtmlEscape="true">${credential.title}</spring:htmlEscape>
                    </div>
                    <div class="credential-publickey">
                        <div class="credential-publickey-title">
                            Public Key
                        </div>
                        <div class="credential-publickey-key">
                                ${credential.publicKey}
                        </div>
                    </div>
                    <img class="delete-icon" alt="Delete" data-url="${deleteCredentialUrl}" data-id="${credential.credentialID}">
                </div>
            </div>
        </c:forEach>
    </div>
</c:if>
<c:if test="${empty credentials}">
    <div class="webauthn-description">
        <spring:theme code="webauthn.page.empty.credentials"/>
    </div>
</c:if>
<div class="register-section post_credential_url" data-url="${verifyCredentialUrl}">
    <div class="register-headline">
        <spring:theme code="webauthn.page.register.description"/>
    </div>
    <div class="row">
        <div class="webauthn-register-credentials">
            <button type="button" class="webauthn-register-credentials-btn btn btn-default" data-url="${createOptionsUrl}"><spring:theme
                    code="webauthn.register.button"/></button>
        </div>
        <c:if test="${not empty credentials}">
            <div class="webauthn-login" data-url="${verifyAssertionUrl}">
                <button type="button" class="webauthn-login-btn btn btn-default" data-url="${authenticateUrl}" data-action="authenticate"><spring:theme
                        code="webauthn.authenticate.button"/></button>
            </div>
        </c:if>
    </div>
</div>