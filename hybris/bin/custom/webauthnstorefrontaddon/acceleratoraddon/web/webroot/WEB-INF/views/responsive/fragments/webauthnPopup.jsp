<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page trimDirectiveWhitespaces="true" %>

<div class="popUpContainer">
    <div class="popUp-header">
        <div class="row">
            <h3>${popupTitle}</h3>
        </div>
    </div>
    <div class="col-lg-12 popUp-content">
        <p><spring:theme code="${popupContent}"/></p>
    </div>
</div>