<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page trimDirectiveWhitespaces="true" %>

<c:url value="/login/webauthn/authenticate" var="authenticateUrl"/>
<c:url value="/j_spring_webauthn_security_check" var="verifyAssertionUrl"/>

<div class="webauthn-login-container">
    <h1>Web Authn Login Page</h1>
    <div class="webauthn-login" data-url="${verifyAssertionUrl}">
        <button type="button" class="webauthn-login-btn btn btn-default" data-url="${authenticateUrl}" data-action="login"><spring:theme
                code="webauthn.authenticate.button"/></button>
    </div>
</div>