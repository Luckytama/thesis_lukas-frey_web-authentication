package de.sybit.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.sybit.facades.WebAuthnCredentialsFacade;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialRequestOptions;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/login")
public class LoginPageController extends AbstractLoginPageController {

    private static final String WEBAUTHN_LOGIN_CMS_PAGE = "webAuthnLogin";

    private HttpSessionRequestCache httpSessionRequestCache;

    @Resource(name = "webAuthnCredentialsFacade")
    private WebAuthnCredentialsFacade credentialsFacade;

    @RequestMapping("/webauthn")
    public String getCredentialAssertionOptions(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(WEBAUTHN_LOGIN_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WEBAUTHN_LOGIN_CMS_PAGE));
        return getViewForPage(model);
    }

    @RequestMapping(value = "/webauthn/authenticate", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PublicKeyCredentialRequestOptions getCredentialAssertionOptions() {
        return credentialsFacade.makeCredentialAssertionOptions();
    }

    @Override
    protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException {
        return getContentPageForLabelOrId("login");
    }

    @Override
    protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response) {
        if (httpSessionRequestCache.getRequest(request, response) != null) {
            return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
        }
        return "/";
    }

    @Override
    protected String getView() {
        return "pages/account/accountLoginPage";
    }

    @Resource(name = "httpSessionRequestCache")
    public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache) {
        this.httpSessionRequestCache = accHttpSessionRequestCache;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
                          @RequestParam(value = "error", defaultValue = "false") final boolean loginError, final Model model,
                          final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
            throws CMSItemNotFoundException {
        if (!loginError) {
            storeReferer(referer, request, response);
        }
        return getDefaultLoginPage(loginError, session, model);
    }

    private void storeReferer(final String referer, final HttpServletRequest request, final HttpServletResponse response) {
        if (StringUtils.isNotBlank(referer) && !StringUtils.endsWith(referer, "/login")
                && StringUtils.contains(referer, request.getServerName())) {
            httpSessionRequestCache.saveRequest(request, response);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister(@RequestHeader(value = "referer", required = false) final String referer, final RegisterForm form,
                             final BindingResult bindingResult, final Model model, final HttpServletRequest request,
                             final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException {
        getRegistrationValidator().validate(form, bindingResult);
        return processRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
    }

}
