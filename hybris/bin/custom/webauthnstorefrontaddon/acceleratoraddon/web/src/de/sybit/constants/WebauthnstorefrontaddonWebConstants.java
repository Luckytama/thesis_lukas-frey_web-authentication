/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.sybit.constants;

/**
 * Global class for all Webauthnstorefrontaddon web constants. You can add global constants for your extension into this class.
 */
public interface WebauthnstorefrontaddonWebConstants // NOSONAR
{

	public static final String ADD_ON_PREFIX = "addon:";
	public static final String VIEW_PAGE_PREFIX = ADD_ON_PREFIX + "/" + WebauthnstorefrontaddonConstants.EXTENSIONNAME;
	public static final String REDIRECT_PREFIX = "redirect:";

	interface Views {

		interface Pages {
			String AccountLoginPage = VIEW_PAGE_PREFIX + "/pages/account/accountLoginPage";
		}

		interface Fragments {
			String WebAuthnPopUp = VIEW_PAGE_PREFIX + "/fragments/webauthnPopup";
			String WebAuthnRegistrationPopUp = VIEW_PAGE_PREFIX + "/fragments/webauthnRegistrationPopup";
		}
	}

	// implement here constants used by this extension
}
