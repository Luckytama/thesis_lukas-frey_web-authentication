package de.sybit.controllers.pages.account;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.sybit.constants.WebauthnstorefrontaddonWebConstants;
import de.sybit.facades.WebAuthnCredentialsFacade;
import de.sybit.hybrisgen.webauthn.data.AuthenticationResponseData;
import de.sybit.hybrisgen.webauthn.data.AuthenticatorResponseData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredential;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialCreateOptionsData;
import de.sybit.hybrisgen.webauthn.data.PublicKeyCredentialRequestOptions;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "my-account/webauthn")
public class WebAuthnAccountPageController extends AbstractPageController {

    private static final Logger LOG = Logger.getLogger(WebAuthnAccountPageController.class);

    private static final String WEBAUTHN_CMS_PAGE = "webauthn";
    private static final String WEBAUTHN_POPUP_REGISTRATION_FAILURE_TITLE = "webauthn.popup.registration.failure.title";
    private static final String WEBAUTHN_POPUP_AUTHENTICATION_FAILURE_TITLE = "webauthn.popup.authentication.title";

    @Resource(name = "webAuthnCredentialsFacade")
    private WebAuthnCredentialsFacade credentialsFacade;

    @Resource(name = "sessionService")
    private SessionService sessionService;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @Resource(name = "i18NService")
    private I18NService i18NService;

    @Resource(name = "viewResolver")
    private ViewResolver viewResolver;

    @RequestMapping(method = RequestMethod.GET)
    public String getOverviewPage(final Model model) throws CMSItemNotFoundException {
        storeCmsPageInModel(model, getContentPageForLabelOrId(WEBAUTHN_CMS_PAGE));
        setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WEBAUTHN_CMS_PAGE));
        model.addAttribute("credentials", credentialsFacade.getWebAuthnCredentialsFromCurrentUser());
        return getViewForPage(model);
    }

    @RequestMapping(value = "/create-options", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PublicKeyCredentialCreateOptionsData getCredentialCreateOptions() {
        return credentialsFacade.makeCredentialCreateOptions();
    }

    @RequestMapping(value = "/verify-credential", method = RequestMethod.POST, consumes = "application/json")
    public String verifyCredentials(final Model model, @RequestBody final AuthenticatorResponseData credential, final BindingResult bindingResult) {
        if (credentialsFacade.finishCredentialRegistration(credential, bindingResult)) {
            return WebauthnstorefrontaddonWebConstants.Views.Fragments.WebAuthnRegistrationPopUp;
        } else {
            final ObjectError firstError = bindingResult.getAllErrors().get(0);
            setPopupContent(model, messageSource.getMessage(WEBAUTHN_POPUP_REGISTRATION_FAILURE_TITLE, null, i18NService.getCurrentLocale()), firstError.getCode());
            return WebauthnstorefrontaddonWebConstants.Views.Fragments.WebAuthnPopUp;
        }
    }

    @RequestMapping(value = "/save-credential", method = RequestMethod.POST)
    public String saveCredentials(@RequestParam("title") final String title) {
        credentialsFacade.storeCredentialsToCurrentUser(sessionService.getAttribute("publicKeyCredential"), title);
        return "redirect:/my-account/webauthn";
    }

    @RequestMapping(value = "/delete-credential", method = RequestMethod.POST)
    @ResponseBody
    public String deleteCredential(@RequestParam("credentialId") final String credentialId) {
        credentialsFacade.deleteCredentialFromCurrentUser(credentialId);
        return String.valueOf(HttpStatus.OK);
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public PublicKeyCredentialRequestOptions getCredentialAssertionOptions() {
        return credentialsFacade.makeCredentialAssertionOptions();
    }

    @RequestMapping(value = "/verify-assertion", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public String verifyAssertion(final Model model, @RequestBody final AuthenticationResponseData authenticationResponse, final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        final PublicKeyCredential getAssertionResponse = credentialsFacade.verifyGetAssertion(authenticationResponse, bindingResult);
        if (getAssertionResponse != null) {
            response.setStatus(HttpStatus.OK.value());
            return credentialsFacade.finishGetAssertion(getAssertionResponse);
        } else {
            final ObjectError firstError = bindingResult.getAllErrors().get(0);
            setPopupContent(model, messageSource.getMessage(WEBAUTHN_POPUP_AUTHENTICATION_FAILURE_TITLE, null, i18NService.getCurrentLocale()), firstError.getCode());
            response.setStatus(HttpStatus.CONFLICT.value());
            renderPopupView(model, request, response);
            return null;
        }
    }

    private void setPopupContent(final Model model, final String title, final String content) {
        model.addAttribute("popupTitle", title);
        model.addAttribute("popupContent", content);
    }

    private void renderPopupView(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            View view = viewResolver.resolveViewName(WebauthnstorefrontaddonWebConstants.Views.Fragments.WebAuthnPopUp, i18NService.getCurrentLocale());
            if (view != null) {
                view.render(model.asMap(), request, response);
            }
        } catch (Exception e) {
            LOG.debug("Could not render popup view", e);
        }
    }
}
